package servico;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/* 
 * Bookstore.java - holds all the data and operations of the bookstore.
 *
 ************************************************************************
 *
 * This is part of the the Java TPC-W distribution,
 * written by Harold Cain, Tim Heil, Milo Martin, Eric Weglarz, and Todd
 * Bezenek.  University of Wisconsin - Madison, Computer Sciences
 * Dept. and Dept. of Electrical and Computer Engineering, as a part of
 * Prof. Mikko Lipasti's Fall 1999 ECE 902 course.
 *
 * Copyright © 2008 Gustavo Maciel Dias Vieira
 *
 * This source code is distributed "as is" in the hope that it will be
 * useful.  It comes with no warranty, and no author or distributor
 * accepts any responsibility for the consequences of its use.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this code under the following conditions:
 *
 * This code is distributed for non-commercial use only.
 * Please contact the maintainer for restrictions applying to 
 * commercial use of these tools.
 *
 * Permission is granted to anyone to make or distribute copies
 * of this code, either as received or modified, in any
 * medium, provided that all copyright notices, permission and
 * nonwarranty notices are preserved, and that the distributor
 * grants the recipient permission for further redistribution as
 * permitted by this document.
 *
 * Permission is granted to distribute this code in compiled
 * or executable form under the same conditions that apply for
 * source code, provided that either:
 *
 * A. it is accompanied by the corresponding machine-readable
 *    source code,
 * B. it is accompanied by a written offer, with no time limit,
 *    to give anyone a machine-readable copy of the corresponding
 *    source code in return for reimbursement of the cost of
 *    distribution.  This written offer must permit verbatim
 *    duplication by anyone, or
 * C. it is distributed by someone who received only the
 *    executable form, and is accompanied by a copy of the
 *    written offer of source code that they received concurrently.
 *
 * In other words, you are welcome to use, share and improve this codes.
 * You are forbidden to forbid anyone else to use, share and improve what
 * you give them.
 *
 ************************************************************************/
import dominio.Address;
import dominio.Author;
import dominio.Book;
import dominio.BookRating;
import dominio.CCTransaction;
import dominio.Cart;
import dominio.Country;
import dominio.Customer;
import dominio.Order;
import dominio.OrderLine;
import dominio.Stock;
import recommendation.Recommendation;
import recommendation.RecommendationImpl;
import util.TPCW_Util;

/**
 * Bookstore.java - classe responsável por uma <code>Bookstore</code>.
 * 
 * <p>Esta classe possuí atributos/métodos estáticos e não estáticos.</p>
 * <p>Esta classe agrega todas as classes do domínio.</p>
 * 
 * <br>
 * <img src="./doc-files/Bookstore.png" alt="Bookstore"> <br>
 * <a href="./doc-files/Bookstore.html"> code </a>
 *
 */
public class Bookstore implements Serializable {

	private static final long serialVersionUID = -3099048826035606338L;
	private static final Logger logger = LoggerFactory.getLogger(Bookstore.class);

    private static boolean populated;
    private static final List<Country> countryById;
    private static final Map<String, Country> countryByName;
    private static final List<Address> addressById;
    private static final Map<Address, Address> addressByAll;
    private static final List<Customer> customersById;
    private static final Map<String, Customer> customersByUsername;
    private static final List<Author> authorsById;
    private static final List<Book> booksById;
	private static final List<BookRating> bookRatingsById;
	private static final LinkedList<String> datasetFilesByDate;
    private final Map<Book, Stock> stockByBook;
    private final List<Cart> cartsById;
    private final List<Order> ordersById;
    private final LinkedList<Order> ordersByCreation;
    private final int id;
    
    private static Recommendation recommendation = new RecommendationImpl();

    /**
     * Bloco static que executa a inicialização dos atributos estáticos da <code>Bookstore</code>
	 * 
     * <pre>
     * <span class="literal">static</span> {
     *          <span class="ST2">countryById</span> = <span class="literal">new</span> ArrayList&lt;&gt;();
     *          <span class="ST2">countryByName</span> = <span class="literal">new</span> HashMap&lt;&gt;();
     *          <span class="ST2">addressById</span> = <span class="literal">new</span> ArrayList&lt;&gt;();
     *          <span class="ST2">addressByAll</span> = <span class="literal">new</span> HashMap&lt;&gt;();
     *          <span class="ST2">customersById</span> = <span class="literal">new</span> ArrayList&lt;&gt;();
     *          <span class="ST2">customersByUsername</span> = <span class="literal">new</span> HashMap&lt;&gt;();
     *          <span class="ST2">authorsById</span> = <span class="literal">new</span> ArrayList&lt;&gt;();
     *          <span class="ST2">booksById</span> = <span class="literal">new</span> ArrayList&lt;&gt;();
	 *          <span class="ST2">datasetFilesByDate</span> = <span class="literal">new</span> LinkedList&lt;&gt;();
     *      }
     * </pre>
     */
    static {
        countryById = new ArrayList<>();
        countryByName = new HashMap<>();
        addressById = new ArrayList<>();
        addressByAll = new HashMap<>();
        customersById = new ArrayList<>();
        customersByUsername = new HashMap<>();
        authorsById = new ArrayList<>();
        booksById = new ArrayList<>();
		bookRatingsById = new ArrayList<>();
		datasetFilesByDate = new LinkedList<>();
    }

    /**
	 * Construtor da classe. Atribui um ID para a <code>Bookstore</code> e inicializa seus parâmetros.
     *
     * @param id o id para um <code>Bookstore</code>.
     */
    public Bookstore(final int id) {
        this.id = id;
        cartsById = new ArrayList<>();
        ordersById = new ArrayList<>();
        ordersByCreation = new LinkedList<>();
        stockByBook = new HashMap<>();
    }

    /**
     * Método para retornar o ID da Bookstore.
     * 
     * @return o id de um <code>Bookstore</code>. 
     */
    public int getId() {
        return id;
    }

    /**
     * Método para retornar de a Bookstore foi populada com dados.
     *
     * @return true se está populada. false caso contrário.
     */
    public boolean isPopulated() {
        return populated;
    }

    /**
     * Método para retornar um país especifico. Caso o país não exista, cria-se um novo.
     *
     * @param name o nome de um país.
     * @return o valor de um <code>country</code>.
     */
    private static Country alwaysGetCountry(String name) {
        Country country = countryByName.get(name);
        if (country == null) {
            country = createCountry(name, "", 0);
        }
        return country;
    }

    /**
     * Retornar simplesmente qualquer país.
     *
     * @param random um objeto <code>Random</code>.
     * @return um valor de um <code>country</code>.
     */
    private static Country getACountryAnyCountry(Random random) {
        return countryById.get(random.nextInt(countryById.size()));
    }

    /**
     * Método para criar um novo país e inserí-lo na lista de países.
     * Também atribui ao pais a ser criado, um id, moeda e abreviação do nome da moeda
	 * 
     * @param name o nome do <code>Country</code>.
     * @param currency a moeda do <code>Country</code>.
     * @param exchange o taxa de câmbio do <code>Country</code>.
     * @return o valor de um <code>country</code> criado.
     */
    private static Country createCountry(String name, String currency, double exchange) {
        int id = countryById.size();
        Country country = new Country(id, name, currency, exchange);
        countryById.add(country);
        countryByName.put(name, country);
        return country;
    }

    /**
     * Retorna um endereço especificado. Cria-se o endereço caso não exista.
     *
     * @param street1 o endereço 1 do <code>Address</code>
     * @param street2 o endereço 2 do <code>Address</code>
     * @param city a cidade do <code>Address</code>
     * @param state o estado do <code>Address</code>
     * @param zip o zip code do <code>Address</code>
     * @param countryName o nome do país do <code>Address</code>
     * @return address
     */
    public static Address alwaysGetAddress(String street1, String street2,
            String city, String state, String zip, String countryName) {
        Country country = alwaysGetCountry(countryName);
        Address key = new Address(0, street1, street2, city, state, zip, country);
        Address address = addressByAll.get(key);
        if (address == null) {
            address = createAddress(street1, street2, city, state, zip,
                    country);
        }
        return address;
    }

    /**
     * Retorna qualquer endereço
     * 
     * @param random retorna um endereço randomico do <code>Address</code>
     * @return randomAddress
     */
    private static Address getAnAddressAnyAddress(Random random) {
        return addressById.get(random.nextInt(addressById.size()));
    }

    private static Address createAddress(String street1, String street2,
            String city, String state, String zip, Country country) {
        int id = addressById.size();
        Address address = new Address(id, street1, street2, city, state, zip,
                country);
        addressById.add(address);
        addressByAll.put(address, address);
        return address;
    }

    /**
     * Método para retornar um cliente baseado em seu ID
     * 
     * @param cId o id do <code>Customer</code> baseado no cliente
     * @return customer
     */
    public static Customer getCustomer(int cId) {
        return customersById.get(cId);
    }

    /**
     * Método para retornar um cliente baseado em seu usuário
     * 
     * @param username o username de um cliente no <code>Optional</code>
     * @return customer
     */
    public static Optional<Customer> getCustomer(String username) {
        return Optional.ofNullable(customersByUsername.get(username));
    }

    private Customer getACustomerAnyCustomer(Random random) {
        return customersById.get(random.nextInt(customersById.size()));
    }

    /**
     * Método para cadastro de um novo cliente
     *
     * @param fname o nome do <code>Customer</code>.
     * @param lname o sobrenome do <code>Customer</code>.
     * @param street1 o endereço 1 do <code>Customer</code>.
     * @param street2 o endereço 2 do <code>Customer</code>.
     * @param city a cidade do <code>Customer</code>.
     * @param state o Estado do <code>Customer</code>.
     * @param zip o CEP do <code>Customer</code>.
     * @param countryName  o nome do País do <code>Customer</code>.
     * @param phone o telefone do <code>Customer</code>.
     * @param email o email do <code>Customer</code>.
     * @param discount o valor do desconto para o <code>Customer</code>.
     * @param birthdate a data de nascimento do <code>Customer</code>.
     * @param data informações adicionais sobre o <code>Customer</code>.
     * @param now  o timestamp de criação.
     * @return customerCreation
     */
    public static Customer createCustomer(String fname, String lname, String street1,
            String street2, String city, String state, String zip,
            String countryName, String phone, String email, double discount,
            Date birthdate, String data, long now) {
        Address address = alwaysGetAddress(street1, street2, city, state, zip,
                countryName);
        return createCustomer(fname, lname, address, phone, email,
                new Date(now), new Date(now), new Date(now),
                new Date(now + 7200000 /* 2 hours */), discount, birthdate,
                data);
    }

    private static Customer createCustomer(String fname, String lname, Address address,
            String phone, String email, Date since, Date lastVisit,
            Date login, Date expiration, double discount, Date birthdate,
            String data) {
        int id = customersById.size();
        String uname = TPCW_Util.DigSyl(id, 0);
        Customer customer = new Customer(id, uname, uname.toLowerCase(), fname,
                lname, phone, email, since, lastVisit, login, expiration,
                discount, 0, 0, birthdate, data, address);
        customersById.add(customer);
        customersByUsername.put(uname, customer);
        return customer;
    }

    /**
     * Método para atualizar a sessão do usuário,
     * criando uma nova data de expiração da sessão
     *
     * @param cId o id do <code>Customer</code> baseado no cliente
     * @param now o timestamp de criação.
     */
    public static void refreshCustomerSession(int cId, long now) {
        Customer customer = getCustomer(cId);
        if (customer != null) {
            customer.setLogin(new Date(now));
            customer.setExpiration(new Date(now + 7200000 /* 2 hours */));
        }
    }

    private static Author getAnAuthorAnyAuthor(Random random) {
        return authorsById.get(random.nextInt(authorsById.size()));
    }

    private static Author createAuthor(String fname, String mname, String lname,
            Date birthdate, String bio) {
        Author author = new Author(fname, mname, lname, birthdate, bio);
        authorsById.add(author);
        return author;
    }

    /**
     * Método para retornar um livro especificando seu bookId
     * 
     * @param bId o id de um book em <code>Optional</code>
     * @return book
     */
    public static Optional<Book> getBook(int bId) {
        return Optional.ofNullable(booksById.get(bId));
    }
    
    /**
     * Método para retornar uma lista de avaliações dos livros
     * 
     * @return listBooks
     */
    public static List<BookRating> getBookRatings(){
    	return bookRatingsById;
	}
	
	/**
     * Método para retornar uma nota para um livro de um <code>Customer</code>.
     * 
	 * @param cId o id de um <code>Customer</code>
	 * @param bId o id de um <code>Book</code>
     * @return um objeto <code>Optional</code> do tipo <code>BookRating</code>.
     */
    public static Optional<BookRating> getBookRating(int cId, int bId){
		return bookRatingsById.stream()
			.filter(bookRating -> bookRating.getCustomer().getId() == cId 
				&& bookRating.getBook().getId() == bId)
			.findFirst();
	}

	/**
	 * Acessor estático para o atributo <code>datasetFilesByDate</code>.
	 * 
	 * @return a lista de arquivos gerados de dataset.
	 */
	public static List<String> getDatasetFiles() {
		return datasetFilesByDate;
	}

    /**
     * Método para retornar recomendações de livros baseadas
     * nos itens de um cliente
     *
     * @param c_id o id de um <code>Customer</code>
     * @return recommendedsByIten
     */
    public static List<Book> getRecommendationByItens(int c_id) {
        return recommendation.itemBased(c_id);
    }

    /**
     * Método para retornar recomendações de livros beseadas
     * na comparação dos gostos do usuários com relação aos outros 
     * clientes
     * 
     * @param c_id o Id de Book baseado na recomendação de usuário em <code>List</code>
     * @return recommdedByUsers
     */
    public static List<Book> getRecommendationByUsers(int c_id) {
        return recommendation.userBased(c_id);
    }

    /**
     * Retornar um livro aleatório 
     * 
     * @param random retorna um livro randomico do <code>Book</code>
     * @return book
     */
    public static Book getABookAnyBook(Random random) {
        return booksById.get(random.nextInt(booksById.size()));
    }

    /**
     * Retorna uma lista de livros sobre um tema especificado
     *
     * @param subject tema de um livro em <code>List</code>
     * @return listaLivros
     */
    public static List<Book> getBooksBySubject(String subject) {
        ArrayList<Book> books = new ArrayList<>();
        for (Book book : booksById) {
            if (subject.equals(book.getSubject())) {
                books.add(book);
                if (books.size() > 50) {
                    break;
                }
            }
        }
        Collections.sort(books, (Book a, Book b) -> a.getTitle().compareTo(b.getTitle()));
        return books;
    }

    /**
     * Retorna uma lista de livros que contenha o titulo especificado
     * 
     * @param title titulo específico de uma book list
     * @return listaLivros
     */
    public static List<Book> getBooksByTitle(String title) {
        Pattern regex = Pattern.compile("^" + title);
        ArrayList<Book> books = new ArrayList<>();
        for (Book book : booksById) {
            if (regex.matcher(book.getTitle()).matches()) {
                books.add(book);
                if (books.size() > 50) {
                    break;
                }
            }
        }
        Collections.sort(books, (Book a, Book b) -> a.getTitle().compareTo(b.getTitle()));
        return books;
    }

    /**
     * Retorna uma lista de livros filtrados pelo autor especificado
     *
     * @param author o autor de books em um filter
     * @return listaLivros
     */
    public static List<Book> getBooksByAuthor(String author) {
        Pattern regex = Pattern.compile("^" + author);
        ArrayList<Book> books = new ArrayList<>();
        for (Book book : booksById) {
            if (regex.matcher(book.getAuthor().getLname()).matches()) {
                books.add(book);
                if (books.size() > 50) {
                    break;
                }
            }
        }
        Collections.sort(books, (Book a, Book b) -> a.getTitle().compareTo(b.getTitle()));
        return books;
    }

    /**
     * Retorna os livros mais recentes do assunto especificado
     *
     * @param subject o subject de livros de uma <code>List</code>
     * @return listaLivrosRecentes
     */
    public static List<Book> getNewBooks(String subject) {
        return getNewBooks0(subject);
    }

    static List<Book> getNewBooks0(String subject) {
        ArrayList<Book> books = new ArrayList<>();
        for (Book book : booksById) {
            if (subject.equals(book.getSubject())) {
                books.add(book);
            }
        }
        Collections.sort(books, new Comparator<Book>() {
            public int compare(Book a, Book b) {
                int result = b.getPubDate().compareTo(a.getPubDate());
                if (result == 0) {
                    result = a.getTitle().compareTo(b.getTitle());
                }
                return result;
            }
        });
        return books.subList(0, books.size() >= 50 ? 50 : books.size());
    }

    /**
     * Classe estática para auxiliar como um contador.
	 * <br>
	 * *<img src="./doc-files/Counter.png" alt="Counter">
     */
    protected static class Counter {

        public Book book; // atributo que representa um <code>Book</code>

        public int count; // atributo que representa um contador.
    }

    /**
     * Método para retornar os livros mais vendidos de uma <code>Bookstore</code>.
     * 
     * <p>Este método segue o seguinte algoritmo: </p>
     * <ol>
     *   <li>Monta-se um <code>Map</code> com os livros e a quantidade total de vendas;</li>
     *   <li>Transforma-se o <code>Map</code> em uma lista e ordena-se por vendas;</li>
     *   <li>Monta-se um novo <code>Map</code> ordenado e retornamos.</li>
     * </ol>
     * 
     * @return bestSellers
     */
    public Map<Book, Integer> getBestSellers() {
    	Map<Book, Integer> booksWithQty = new HashMap<>();
		ordersByCreation.stream()
			.filter(order -> order.getStatus().equals("SHIPPED"))
			.flatMap(order -> order.getLines().stream())
			.forEach(orderLine -> {
				Book book = orderLine.getBook();
				if (!booksWithQty.containsKey(book)) {
					booksWithQty.put(book, orderLine.getQty());
				} else {
					booksWithQty.put(book, (booksWithQty.get(book) + orderLine.getQty()));
				}
			});
		
		List<Map.Entry<Book, Integer>> list =
                new LinkedList<>(booksWithQty.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<Book, Integer>>() {
            public int compare(Map.Entry<Book, Integer> entry1,
                               Map.Entry<Book, Integer> entry2) {
            	int result = entry2.getValue() - entry1.getValue();
				if (result == 0) {
					result = entry1.getKey().getTitle().compareTo(entry2.getKey().getTitle());
				}
				return result;
            }
        });
		
		Map<Book, Integer> bestSellers = new LinkedHashMap<>();
        for (Map.Entry<Book, Integer> entry : list.subList(0, list.size() >= 100 ? 100 : list.size())) {
        	bestSellers.put(entry.getKey(), entry.getValue());
        }
        
        return bestSellers;
    }

	/**
	 * Retorna uma lista de pedidos 
	 * 
	 * @return a lista de pedidos.
	 */
	public List<Order> getOrdersById() {
		return ordersById;
	}

	private static Book createBook(String title, Date pubDate, String publisher, String subject, String desc,
			String thumbnail, String image, double srp, Date avail, String isbn, int page, String backing,
			String dimensions, Author author) {
		int id = booksById.size();
		Book book = new Book(id, title, pubDate, publisher, subject, desc, thumbnail, image, srp, avail, isbn, page,
				backing, dimensions, author);
		booksById.add(book);
		return book;
	}

	/**
	 * Método para atualizar um <code>Book</code>.
	 * 
	 * @param bId o id de um <code>Book</code>.
	 * @param image o caminho para uma imagem.
	 * @param thumbnail o caminho para um thumbnail.
	 * @param now o timestamp de atualização.
	 */
	public static void updateBook(int bId, String image, String thumbnail, long now) {
		Book book = getBook(bId).get();
		book.setImage(image);
		book.setThumbnail(thumbnail);
		book.setPubDate(new Date(now));
		// updateRelatedBooks(book);
	}

	/**
	 * Método para atualizar o estoque (<code>Stock</code>) de um livro.
	 * 
	 * @param bId o id do <code>Book</code>.
	 * @param cost o novo valor do custo do <code>Stock</code>.
	 */
	public void updateStock(int bId, double cost) {
		Book book = getBook(bId).get();
		if (!stockByBook.containsKey(book)) {
			int stock = TPCW_Util.getRandomInt(rand, 10, 30);
			stockByBook.put(book, new Stock(this.id, book, cost, stock));
		}
		stockByBook.get(book).setCost(cost);
	}

	/**
	 * Método acessor que retorna o <code>Stock</code> de um livro.
	 * 
	 * @param bId o id do <code>Book</code>.
	 * @return o objeto <code>Stock</code> do livro.
	 */
	public Stock getStock(int bId) {
		final Book book = getBook(bId).get();
		return stockByBook.get(book);
	}

	/**
	 * For all the clients that bought this book in the last 10000 orders, what are
	 * the five most sold books except this one.
	 * 
	 * @param targetBook the Book that should be assigned the related books.
	 */
	private void updateRelatedBooks(Book targetBook) {
		HashSet<Integer> clientIds = new HashSet<>();
		int j = 0;
		Iterator<Order> i = ordersByCreation.iterator();
		while (i.hasNext() && j <= 10000) {
			Order order = i.next();
			for (OrderLine line : order.getLines()) {
				Book book = line.getBook();
				if (targetBook.getId() == book.getId()) {
					clientIds.add(order.getCustomer().getId());
					break;
				}
			}
			j++;
		}
		HashMap<Integer, Counter> counters = new HashMap<>();
		i = ordersByCreation.iterator();
		while (i.hasNext()) {
			Order order = i.next();
			if (clientIds.contains(order.getCustomer().getId())) {
				order.getLines().forEach((line) -> {
					Book book = line.getBook();
					if (targetBook.getId() != book.getId()) {
						Counter counter = counters.get(book.getId());
						if (counter == null) {
							counter = new Counter();
							counter.book = book;
							counter.count = 0;
							counters.put(book.getId(), counter);
						}
						counter.count += line.getQty();
					}
				});
			}
		}
		Counter[] sorted = counters.values().toArray(new Counter[] {});
		Arrays.sort(sorted, (Counter a, Counter b) -> {
			if (b.count > a.count) {
				return 1;
			}
			return b.count < a.count ? -1 : 0;
		});
		Book[] related = new Book[] { targetBook, targetBook, targetBook, targetBook, targetBook };
		for (j = 0; j < 5 && j < sorted.length; j++) {
			related[j] = sorted[j].book;
		}
		targetBook.setRelated1(related[0]);
		targetBook.setRelated2(related[1]);
		targetBook.setRelated3(related[2]);
		targetBook.setRelated4(related[3]);
		targetBook.setRelated5(related[4]);
	}

	/**
	 * Método para retornar um carrinho <code>Cart</code> por <code>id</code>.
	 * 
	 * @param id o id do <code>Cart</code>.
	 * @return o objeto <code>Cart</code>.
	 */
	public Cart getCart(int id) {
		return cartsById.get(id);
	}

	/**
	 * Método para criar um carrinho <code>Cart</code> vazio.
	 * 
	 * @param now o timestamp da criação.
	 * @return o novo objeto <code>Cart</code> criado.
	 */
	public Cart createCart(long now) {
		int idCart = cartsById.size();
		Cart cart = new Cart(idCart, new Date(now));
		cartsById.add(cart);
		return cart;
	}

	/**
	 * Método para atualizar o carrinho <code>Cart</code>.
	 * 
	 * @param cId        o id do <code>Cart</code>.
	 * @param bId        o id do <code>Book</code>.
	 * @param bIds       os ids de cada <code>Book</code>.
	 * @param quantities a quantidade de cada id de <code>Book</code>.
	 * @param now        o timestamp da atualização.
	 * @return o objeto <code>Cart</code> atualizado.
	 */
	public Cart cartUpdate(int cId, Integer bId, List<Integer> bIds, List<Integer> quantities, long now) {
		Cart cart = getCart(cId);

		if (bId != null) {
			cart.increaseLine(stockByBook.get(getBook(bId).get()), getBook(bId).get(), 1);
		}

		for (int i = 0; i < bIds.size(); i++) {
			cart.changeLine(stockByBook.get(getBook(bId).get()), booksById.get(bIds.get(i)), quantities.get(i));
		}

		cart.setTime(new Date(now));

		return cart;
	}

	/**
	 * Método para confirmar uma compra. 
	 * 
	 * <p>Caso o parâmetro <code>addressId</code> seja -1, utiliza-se o endereço do <code>Customer</code>.</p>
	 * <p>Para cada <code>Book</code> incluído na compra, que o <code>Stock</code> associado estiver
	 * <br> menor que 10, adicionar 21 unidades à quantidade.</p>
	 * 
	 * @param customerId   o id do <code>Customer</code>.
	 * @param cartId       o id do <code>Cart</code>.
	 * @param comment      o comment da compra.
	 * @param ccType       o tipo do cartão de crédito.
	 * @param ccNumber     o número do cartão de crédito.
	 * @param ccName       o nome do titular do cartão de crédito.
	 * @param ccExpiry     a data de expiração do cartão de crédito.
	 * @param shipping     o tipo de entrega.
	 * @param shippingDate a data para entrega.
	 * @param addressId    o id do <code>Address</code>.
	 * @param now          o timestamp da compra.
	 * @return o <code>Order</code> criado.
	 */
	public Order confirmBuy(int customerId, int cartId, String comment, String ccType, long ccNumber, String ccName,
			Date ccExpiry, String shipping, Date shippingDate, int addressId, long now) {
		Customer customer = getCustomer(customerId);
		Cart cart = getCart(cartId);
		Address shippingAddress = customer.getAddress();
		if (addressId != -1) {
			shippingAddress = addressById.get(addressId);
		}
		cart.getLines().stream().map((cartLine) -> {
			Book book = cartLine.getBook();
			stockByBook.get(book).addQty(-cartLine.getQty());
			return book;
		}).filter((book) -> (stockByBook.get(book).getQty() < 10)).forEachOrdered((book) -> {
			stockByBook.get(book).addQty(21);
		});
		CCTransaction ccTransact = new CCTransaction(ccType, ccNumber, ccName, ccExpiry, "",
				cart.total(customer.getDiscount()), new Date(now), shippingAddress.getCountry());
		return createOrder(customer, new Date(now), cart, comment, shipping, shippingDate, "Pending",
				customer.getAddress(), shippingAddress, ccTransact);
	}

	private Order createOrder(Customer customer, Date date, Cart cart, String comment, String shipType, Date shipDate,
			String status, Address billingAddress, Address shippingAddress, CCTransaction cc) {
		int idOrder = ordersById.size();
		Order order = new Order(idOrder, customer, date, cart, comment, shipType, shipDate, status, billingAddress,
				shippingAddress, cc);
		ordersById.add(order);
		ordersByCreation.addFirst(order);
		customer.logOrder(order);
		cart.clear();
		return order;
	}

	/**
	 * Método para um <code>Customer</code> avaliar um <code>Book</code>.
	 * 
	 * <p>Se já existe um <code>BookRating</code> associado ao <code>Customer</code> e ao <code>Book</code>, 
	 * a nota é atualizada.</p>
	 * <p>Ao tentar criar uma nota, se a nota for menor que 0 ou maior que 5, retorna <code>null</code>.</p>
	 * <p>Ao tentar atualizar uma nota, se a nota for menor que 0 ou maior que 5, retorna o objeto <code>BookRating</code> sem alteração.</p>
	 * 
	 * @param customerId o id do <code>Customer</code>.
	 * @param bookId o id do <code>Book</code>.
	 * @param rating a nota para o item.
	 * @return o objeto <code>BookRating</code> criado/atualizado.
	 */
	public static BookRating rateABook(int customerId, int bookId, double rating) {
		Optional<BookRating> optional = getBookRating(customerId, bookId);
		BookRating bookRating = null;
		
		if (optional.isPresent()) {
			bookRating = optional.get();
			if (rating < 0.0 || rating > 5.0) {
				System.out.println("Não foi possível atualizar a nota. Nota deve ser entre 0 e 5.");
				return bookRating;
			}
			bookRating.setRating(rating);
		} else {
			if (rating < 0.0 || rating > 5.0) {
				System.out.println("Não foi possível dar uma nota. Nota deve ser entre 0 e 5.");
				return bookRating;
			}
			Customer customer = getCustomer(customerId);
			Book book = getBook(bookId).get();
			int bookRatingId = bookRatingsById.size();
			bookRating = new BookRating(bookRatingId, customer, book, rating);
			bookRatingsById.add(bookRating);
		}
		
		return bookRating;
	}

	private static Random rand;

	/**
	 * Método estático para popular os atributos estático de um <code>Bookstore</code>.
	 * 
	 * <p>Este método deve ser invocado uma única vez. Ao final do método seta o atributo
	 * <br> <code>populated</code> para <code>true</code>. Se este método for invocado novamente,
	 * <br> retorna <code>false</code>.</p>
	 * 
	 * @param seed      o seed para auxiliar na população da plataforma de e-commerce.
	 * @param now       o timestamp para auxiliar na população da plataforma de e-commerce.
	 * @param items     a quantidade de itens da plataforma de e-commerce.
	 * @param customers a quantidade de customers da plataforma de e-commerce.
	 * @param addresses a quantidade de addresses da plataforma de e-commerce.
	 * @param authors   a quantidade de authors da plataforma de e-commerce.
	 * @return false, se já está populado. true, ao acabar de popular. 
	 */
	public static boolean populate(long seed, long now, int items, int customers, int addresses, int authors) {
		if (populated) {
			return false;
		}
		System.out.println("Beginning TPCW population.");
		rand = new Random(seed);
		populateCountries();
		populateAddresses(addresses, rand);
		populateCustomers(customers, rand, now);
		populateAuthorTable(authors, rand);
		populateBooks(items, rand);
		populated = true;
		System.out.println("Finished TPCW population.");
		return true;
	}

	private static void populateCountries() {
		String[] countries = { "United States", "United Kingdom", "Canada", "Germany", "France", "Japan", "Netherlands",
				"Italy", "Switzerland", "Australia", "Algeria", "Argentina", "Armenia", "Austria", "Azerbaijan",
				"Bahamas", "Bahrain", "Bangla Desh", "Barbados", "Belarus", "Belgium", "Bermuda", "Bolivia", "Botswana",
				"Brazil", "Bulgaria", "Cayman Islands", "Chad", "Chile", "China", "Christmas Island", "Colombia",
				"Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Dominican Republic", "Eastern Caribbean",
				"Ecuador", "Egypt", "El Salvador", "Estonia", "Ethiopia", "Falkland Island", "Faroe Island", "Fiji",
				"Finland", "Gabon", "Gibraltar", "Greece", "Guam", "Hong Kong", "Hungary", "Iceland", "India",
				"Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Jamaica", "Jordan", "Kazakhstan", "Kuwait",
				"Lebanon", "Luxembourg", "Malaysia", "Mexico", "Mauritius", "New Zealand", "Norway", "Pakistan",
				"Philippines", "Poland", "Portugal", "Romania", "Russia", "Saudi Arabia", "Singapore", "Slovakia",
				"South Africa", "South Korea", "Spain", "Sudan", "Sweden", "Taiwan", "Thailand", "Trinidad", "Turkey",
				"Venezuela", "Zambia" };

		double[] exchanges = { 1, .625461, 1.46712, 1.86125, 6.24238, 121.907, 2.09715, 1842.64, 1.51645, 1.54208,
				65.3851, 0.998, 540.92, 13.0949, 3977, 1, .3757, 48.65, 2, 248000, 38.3892, 1, 5.74, 4.7304, 1.71, 1846,
				.8282, 627.1999, 494.2, 8.278, 1.5391, 1677, 7.3044, 23, .543, 36.0127, 7.0707, 15.8, 2.7, 9600,
				3.33771, 8.7, 14.9912, 7.7, .6255, 7.124, 1.9724, 5.65822, 627.1999, .6255, 309.214, 1, 7.75473, 237.23,
				74.147, 42.75, 8100, 3000, .3083, .749481, 4.12, 37.4, 0.708, 150, .3062, 1502, 38.3892, 3.8, 9.6287,
				25.245, 1.87539, 7.83101, 52, 37.8501, 3.9525, 190.788, 15180.2, 24.43, 3.7501, 1.72929, 43.9642,
				6.25845, 1190.15, 158.34, 5.282, 8.54477, 32.77, 37.1414, 6.1764, 401500, 596, 2447.7 };

		String[] currencies = { "Dollars", "Pounds", "Dollars", "Deutsche Marks", "Francs", "Yen", "Guilders", "Lira",
				"Francs", "Dollars", "Dinars", "Pesos", "Dram", "Schillings", "Manat", "Dollars", "Dinar", "Taka",
				"Dollars", "Rouble", "Francs", "Dollars", "Boliviano", "Pula", "Real", "Lev", "Dollars", "Franc",
				"Pesos", "Yuan Renmimbi", "Dollars", "Pesos", "Kuna", "Pesos", "Pounds", "Koruna", "Kroner", "Pesos",
				"Dollars", "Sucre", "Pounds", "Colon", "Kroon", "Birr", "Pound", "Krone", "Dollars", "Markka", "Franc",
				"Pound", "Drachmas", "Dollars", "Dollars", "Forint", "Krona", "Rupees", "Rupiah", "Rial", "Dinar",
				"Punt", "Shekels", "Dollars", "Dinar", "Tenge", "Dinar", "Pounds", "Francs", "Ringgit", "Pesos",
				"Rupees", "Dollars", "Kroner", "Rupees", "Pesos", "Zloty", "Escudo", "Leu", "Rubles", "Riyal",
				"Dollars", "Koruna", "Rand", "Won", "Pesetas", "Dinar", "Krona", "Dollars", "Baht", "Dollars", "Lira",
				"Bolivar", "Kwacha" };

		System.out.print("Creating " + countries.length + " countries...");

		for (int i = 0; i < countries.length; i++) {
			createCountry(countries[i], currencies[i], exchanges[i]);
		}

		System.out.println(" Done");
	}

	private static void populateAddresses(int number, Random rand) {
		System.out.print("Creating " + number + " addresses...");

		for (int i = 0; i < number; i++) {
			if (i % 10000 == 0) {
				System.out.print(".");
			}
			Country country = getACountryAnyCountry(rand);
			createAddress(TPCW_Util.getRandomString(rand, 15, 40), TPCW_Util.getRandomString(rand, 15, 40),
					TPCW_Util.getRandomString(rand, 4, 30), TPCW_Util.getRandomString(rand, 2, 20),
					TPCW_Util.getRandomString(rand, 5, 10), country);
		}

		System.out.println(" Done");
	}

	private static void populateCustomers(int number, Random rand, long now) {
		System.out.print("Creating " + number + " customers...");

		for (int i = 0; i < number; i++) {
			if (i % 10000 == 0) {
				System.out.print(".");
			}
			Address address = getAnAddressAnyAddress(rand);
			long since = now - TPCW_Util.getRandomInt(rand, 1, 730) * 86400000 /* a day */;
			long lastLogin = since + TPCW_Util.getRandomInt(rand, 0, 60) * 86400000 /* a day */;
			createCustomer(TPCW_Util.getRandomString(rand, 8, 15), TPCW_Util.getRandomString(rand, 8, 15), address,
					TPCW_Util.getRandomString(rand, 9, 16),
					TPCW_Util.getRandomString(rand, 2, 9) + "@" + TPCW_Util.getRandomString(rand, 2, 9) + ".com",
					new Date(since), new Date(lastLogin), new Date(now), new Date(now + 7200000 /* 2 hours */),
					rand.nextInt(51), TPCW_Util.getRandomBirthdate(rand), TPCW_Util.getRandomString(rand, 100, 500));
		}

		System.out.println(" Done");
	}

	private static void populateAuthorTable(int number, Random rand) {
		System.out.print("Creating " + number + " authors...");

		for (int i = 0; i < number; i++) {
			if (i % 10000 == 0) {
				System.out.print(".");
			}
			createAuthor(TPCW_Util.getRandomString(rand, 3, 20), TPCW_Util.getRandomString(rand, 1, 20),
					TPCW_Util.getRandomLname(rand), TPCW_Util.getRandomBirthdate(rand),
					TPCW_Util.getRandomString(rand, 125, 500));
		}

		System.out.println(" Done");
	}

	private static final String[] SUBJECTS = { "ARTS", "BIOGRAPHIES", "BUSINESS", "CHILDREN", "COMPUTERS", "COOKING",
			"HEALTH", "HISTORY", "HOME", "HUMOR", "LITERATURE", "MYSTERY", "NON-FICTION", "PARENTING", "POLITICS",
			"REFERENCE", "RELIGION", "ROMANCE", "SELF-HELP", "SCIENCE-NATURE", "SCIENCE_FICTION", "SPORTS", "YOUTH",
			"TRAVEL" };
	private static final String[] BACKINGS = { "HARDBACK", "PAPERBACK", "USED", "AUDIO", "LIMITED-EDITION" };

	private static void populateBooks(int number, Random rand) {

		System.out.print("Creating " + number + " books...");

		for (int i = 0; i < number; i++) {
			if (i % 10000 == 0) {
				System.out.print(".");
			}
			Author author = getAnAuthorAnyAuthor(rand);
			Date pubdate = TPCW_Util.getRandomPublishdate(rand);
			double srp = TPCW_Util.getRandomInt(rand, 100, 99999) / 100.0;
			String subject = SUBJECTS[rand.nextInt(SUBJECTS.length)];
			String title = subject + " " + TPCW_Util.getRandomString(rand, 14, 60);
			createBook(title, pubdate, TPCW_Util.getRandomString(rand, 14, 60), SUBJECTS[rand.nextInt(SUBJECTS.length)],
					TPCW_Util.getRandomString(rand, 100, 500), "img" + i % 100 + "/thumb_" + i + ".gif",
					"img" + i % 100 + "/image_" + i + ".gif", srp,
					new Date(pubdate.getTime() + TPCW_Util.getRandomInt(rand, 1, 30) * 86400000 /* a day */),
					TPCW_Util.getRandomString(rand, 13, 13), TPCW_Util.getRandomInt(rand, 20, 9999),
					BACKINGS[rand.nextInt(BACKINGS.length)],
					(TPCW_Util.getRandomInt(rand, 1, 9999) / 100.0) + "x"
							+ (TPCW_Util.getRandomInt(rand, 1, 9999) / 100.0) + "x"
							+ (TPCW_Util.getRandomInt(rand, 1, 9999) / 100.0),
					author);
		}

		for (int i = 0; i < number; i++) {
			Book book = booksById.get(i);
			HashSet<Book> related = new HashSet<>();
			while (related.size() < 5) {
				Book relatedBook = getABookAnyBook(rand);
				if (relatedBook.getId() != i) {
					related.add(relatedBook);
				}
			}
			Book[] relatedArray = new Book[] { booksById.get(TPCW_Util.getRandomInt(rand, 0, number - 1)),
					booksById.get(TPCW_Util.getRandomInt(rand, 0, number - 1)),
					booksById.get(TPCW_Util.getRandomInt(rand, 0, number - 1)),
					booksById.get(TPCW_Util.getRandomInt(rand, 0, number - 1)),
					booksById.get(TPCW_Util.getRandomInt(rand, 0, number - 1)) };
			relatedArray = related.toArray(relatedArray);
			book.setRelated1(relatedArray[0]);
			book.setRelated2(relatedArray[1]);
			book.setRelated3(relatedArray[2]);
			book.setRelated4(relatedArray[3]);
			book.setRelated5(relatedArray[4]);
		}

		System.out.println(" Done");
	}

	void populateInstanceBookstore(int number, Random rand, long now) {
		populateOrders(number, rand, now);
		populateStocks(number, rand, now);
	}

	private void populateStocks(int number, Random rand, long now) {
		System.out.print("Creating " + number + " stocks...");
		for (int i = 0; i < number; i++) {
			if (i % 10000 == 0) {
				System.out.print(".");
			}
			int nBooks = TPCW_Util.getRandomInt(rand, 1, 5);
			for (int j = 0; j < nBooks; j++) {
				Book book = getABookAnyBook(rand);
				if (!stockByBook.containsKey(book)) {
					double cost = TPCW_Util.getRandomInt(rand, 50, 100) / 100.0;
					int quantity = TPCW_Util.getRandomInt(rand, 300, 400);
					stockByBook.put(book, new Stock(this.id, book, cost, quantity));
				}
			}
		}
		System.out.println(" Done");
	}

	private void populateOrders(int number, Random rand, long now) {
		String[] credit_cards = { "VISA", "MASTERCARD", "DISCOVER", "AMEX", "DINERS" };
		String[] ship_types = { "AIR", "UPS", "FEDEX", "SHIP", "COURIER", "MAIL" };
		String[] status_types = { "PROCESSING", "SHIPPED", "PENDING", "DENIED" };

		System.out.print("Creating " + number + " orders...");

		for (int i = 0; i < number; i++) {
			if (i % 10000 == 0) {
				System.out.print(".");
			}
			int nBooks = TPCW_Util.getRandomInt(rand, 1, 5);
			Cart cart = new Cart(-1, null);
			String comment = TPCW_Util.getRandomString(rand, 20, 100);
			for (int j = 0; j < nBooks; j++) {
				Book book = getABookAnyBook(rand);
				int quantity = TPCW_Util.getRandomInt(rand, 1, 300);
				if (!stockByBook.containsKey(book)) {
					double cost = TPCW_Util.getRandomInt(rand, 50, 100) / 100.0;
					int stock = TPCW_Util.getRandomInt(rand, 300, 400);
					stockByBook.put(book, new Stock(this.id, book, cost, stock));
				}
				cart.changeLine(stockByBook.get(book), book, quantity);
			}

			Customer customer = getACustomerAnyCustomer(rand);
			CCTransaction ccTransact = new CCTransaction(credit_cards[rand.nextInt(credit_cards.length)],
					TPCW_Util.getRandomLong(rand, 1000000000000000L, 9999999999999999L),
					TPCW_Util.getRandomString(rand, 14, 30),
					new Date(now + TPCW_Util.getRandomInt(rand, 10, 730) * 86400000 /* a day */),
					TPCW_Util.getRandomString(rand, 15, 15), cart.total(customer.getDiscount()), new Date(now),
					getACountryAnyCountry(rand));
			long orderDate = now - TPCW_Util.getRandomInt(rand, 53, 60) * 86400000 /* a day */;
			long shipDate = orderDate + TPCW_Util.getRandomInt(rand, 0, 7) * 86400000 /* a day */;
			createOrder(customer, new Date(orderDate), cart, comment, ship_types[rand.nextInt(ship_types.length)],
					new Date(shipDate), status_types[rand.nextInt(status_types.length)], getAnAddressAnyAddress(rand),
					getAnAddressAnyAddress(rand), ccTransact);
		}

		System.out.println(" Done");
	}

	private static final Double[] RATINGS_VALUE = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0 };

	/**
	 * Método estático para popular as preferências de usuários sobre os livros.
	 * 
	 * <p>Este método gera randomicamente valores de <code>BookRating</code> associados 
	 * para todas as compras de cada instância de <code>Bookstore</code>.
	 * 
	 * <p>Todos os <code>BookRating</code> gerados, são guardados no atributo estático <code>bookRatingsById</code>.</p>
	 * 
	 * <p>Ao final da geração, é criado um arquivo de dataset para auxiliar nas recomendações de livros</p>
	 * 
	 * @param orders uma lista de <code>Order</code>, que são representa todos os pedidos de uma instância de <code>Bookstore</code>.
	 * @param rand um <code>Random</code> para auxiliar na população.
	 */
	public static void populateEvaluation(List<Order> orders, Random rand) {
		System.out.print("Creating evaluation data for " + orders.size() + " orders...");

		for (Order order : orders) {
			if (order.getId() % 10000 == 0) {
				System.out.print(".");
			}
			Customer customer = order.getCustomer();
			double rating = RATINGS_VALUE[rand.nextInt(RATINGS_VALUE.length)];
			for (OrderLine line : order.getLines()) {
				Book book = line.getBook();
				createBookRating(customer, book, rating);
			}
		}
		
		System.out.println(" Done");
	}

	private static BookRating createBookRating(Customer customer, Book book, double rating) {
		int bookRatingId = bookRatingsById.size();

		BookRating bookRating = new BookRating(bookRatingId, customer, book, rating);
		bookRatingsById.add(bookRating);

		return bookRating;
	}

	/**
	 * Método estático para gerar um arquivo de dataset que será utilizado nas recomendações de livros.
	 * 
	 * <p>Este método trabalha sobre o valor atual de todos os <code>BookRating</code> contido na aplicação.</p>
	 * <p>O formato do arquivo gerado é <span>g_dataset_yyyyMMddHHmmss.txt</span>.
	 */
	public static void generateDataSetFile() {
		System.out.print("Generating a dataset file...");
		
		String date = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		final String GENERATED_DATASET_FILE_PATH = String.format("src/main/resources/g_dataset_%s.txt", date);

		Path file = Paths.get(GENERATED_DATASET_FILE_PATH);
		Map<Integer, List<BookRating>> groupedBookRatings = Bookstore.getBookRatings().stream()
				.collect(Collectors.groupingBy(bookRating -> bookRating.getCustomer().getId()));

		groupedBookRatings.entrySet().forEach(entry -> {
			if (entry.getKey() % 10000 == 0) {
				System.out.print(".");
			}
			try {
				entry.getValue().forEach(bookRating -> {
					try {
						Files.write(file, bookRating.toStringDataset().getBytes(StandardCharsets.UTF_8),
								StandardOpenOption.CREATE, StandardOpenOption.APPEND);
					} catch (IOException e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}
				});
				Files.write(file, System.lineSeparator().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE,
						StandardOpenOption.APPEND);
			} catch (IOException e) {
				logger.error(e.getMessage());
				e.printStackTrace();
			}
		});
		
		datasetFilesByDate.addFirst(GENERATED_DATASET_FILE_PATH);

		System.out.println(" Done");
	}
}
