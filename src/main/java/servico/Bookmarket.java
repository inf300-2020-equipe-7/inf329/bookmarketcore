package servico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dominio.Address;
import dominio.Book;
import dominio.BookRating;
import dominio.Cart;
import dominio.Customer;
import dominio.Order;
import dominio.Stock;
import util.TPCW_Util;

/**
 * Bookmarket.java - classe responsável pelos eventos do e-commerce.
 *
 * <p>
 * Esta classe é composta somente de classes e métodos estáticos.
 * </p>
 * <p>
 * Ela implementa uma máquina de estados (<code>StateMachine</code>) que cuida
 * das operações de {@link Bookstore} da aplicação. Uma instância de
 * <code>Bookstore</code> é representado como um estado da máquina.
 * </p>
 * <p>
 * A interface <code>Action</code> representa um evento/ação. Para a plataforma
 * de e-commerce disparar qualquer evento/ação para mudar o estado das stores,
 * portanto um evento precisa implementar a interface descrita.
 * </p>
 * <br>
 * <img src="./doc-files/Bookmarket.png" alt="Bookmarket" width="1000px"> <br>
 * <a href="./doc-files/Bookmarket.html"> code </a>
 */
public class Bookmarket {

    private interface Action<STATE> {

        Object executeOn(STATE sm);
    }

    private static class StateMachine {

        private final List<Bookstore> state;

        public StateMachine(final List object) {
            this.state = object;
        }

        Object execute(Action action) {
            return action.executeOn(getStateStream());
        }

        void checkpoint() {

        }

        public Stream getStateStream() {
            return state.stream();
        }

        private static StateMachine create(Bookstore... state) {
            List list = new ArrayList();
            list.addAll(Arrays.asList(state));
            return new StateMachine(list);
        }

    }

    private class UmbrellaException extends RuntimeException {

    }

    private static Random random;
    private static StateMachine stateMachine;

    /**
     * Método para inicializar o e-commerce
     * 
     * @param state um <code>Bookstore</code> que será um estado.
     */
    public static void init(Bookstore... state) {
        random = new Random();
        try {
            stateMachine = StateMachine.create(state);
        } catch (UmbrellaException e) {
            throw new RuntimeException(e);
        }
    }

    private static Stream<Bookstore> getBookstoreStream() {
        return (Stream<Bookstore>) stateMachine.getStateStream();
    }

    /**
     * Método estático para retornar um <code>Customer</code>.
     * 
     * @param UNAME o nome de um <code>Customer</code>.
     * @return O objeto <code>Customer</code> com o nome do parâmetro.
     */
    public static Customer getCustomer(String UNAME) {
        return Bookstore.getCustomer(UNAME).get();
    }

    /**
     * Método estático para retornar o nome de um <code>Customer</code>.
     * 
     * @param c_id o id de um <code>Customer</code>.
     * @return O nome do <code>Customer</code> com o parâmetro c_id.
     */
    public static String[] getName(int c_id) {

        Customer customer = Bookstore.getCustomer(c_id);

        String name[] = new String[3];
        name[0] = customer.getFname();
        name[1] = customer.getLname();
        name[2] = customer.getUname();
        return name;
    }

    /**
     * Metodo estático para retornar o username de um <code>Customer</code>.
     *
     * @param C_ID o id de um <code>Customer</code>.
     * @return O username do <code>Customer</code> com o parâmetro C_ID.
     */
    public static String getUserName(int C_ID) {
        return Bookstore.getCustomer(C_ID).getUname();
    }

    /**
     * Método estático para retornar o password de um <code>Customer</code>.
     * 
     * @param C_UNAME o username de um <code>Customer</code>.
     * @return O password do <code>Customer</code> com o parâmetro C_UNAME.
     */
    public static String getPassword(String C_UNAME) {
        return Bookstore.getCustomer(C_UNAME).get().getPasswd();

    }

    /**
     * Método estático para retornar o <code>Order</code> mais recente de um
     * <code>Customer</code>.
     * 
     * @param c_uname o username de um <code>Customer</code>.
     * @return O <code>Order</code> mais recenter do <code>Customer</code> com o
     *         parâmetro c_uname.
     */
    public static Order getMostRecentOrder(String c_uname) {
        return Bookstore.getCustomer(c_uname).get().getMostRecentOrder();
    }

    /**
     * Método estático para criar um novo <code>Customer</code>, além dos parâmetros
     * passados o sistema registra um valor flutuante para o desconto e o horário de
     * criação.
     * 
     * @param fname       o nome do <code>Customer</code>.
     * @param lname       o sobrenome do <code>Customer</code>.
     * @param street1     o endereço 1 do <code>Customer</code>.
     * @param street2     o endereço 2 do <code>Customer</code>.
     * @param city        a cidade do <code>Customer</code>.
     * @param state       o Estado do <code>Customer</code>.
     * @param zip         o CEP do <code>Customer</code>.
     * @param countryName o nome do País do <code>Customer</code>.
     * @param phone       o telefone do <code>Customer</code>.
     * @param email       o email do <code>Customer</code>.
     * @param birthdate   a data de nascimento do <code>Customer</code>.
     * @param data        informações adicionais sobre o <code>Customer</code>.
     * @return O <code>Customer</code> criado com sucesso.
     */
    public static Customer createNewCustomer(String fname, String lname, String street1, String street2, String city,
            String state, String zip, String countryName, String phone, String email, Date birthdate, String data) {
        double discount = (int) (Math.random() * 51);
        long now = System.currentTimeMillis();
        try {
            return (Customer) stateMachine.execute(new CreateCustomerAction(fname, lname, street1, street2, city, state,
                    zip, countryName, phone, email, discount, birthdate, data, now));

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Método estático para atualizar a sessão de um <code>Customer</code>.
     * 
     * @param cId o id do <code>Customer</code>.
     */
    public static void refreshSession(int cId) {
        try {
            stateMachine.execute(new RefreshCustomerSessionAction(cId, System.currentTimeMillis()));

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Método estático para retornar um <code>Book</code>.
     * 
     * @param i_id o id do <code>Book</code>.
     * @return O objeto <code>Book</code> com o parâmetro i_id.
     */
    public static Book getBook(int i_id) {

        return Bookstore.getBook(i_id).get();

    }

    /**
     * Método estático para retornar um <code>Book</code> qualquer.
     * 
     * @return Um objeto <code>Book</code> qualquer.
     */
    public static Book getABookAnyBook() {
        return Bookstore.getABookAnyBook(random);

    }

    /**
     * Método estático para pesquisar por subject.
     * 
     * @param search_key um termo de pesquisa.
     * @return Uma lista de <code>Book</code> com os resultados da pesquisa.
     */
    public static List<Book> doSubjectSearch(String search_key) {
        return Bookstore.getBooksBySubject(search_key);
    }

    /**
     * Método estático para pesquisar por title.
     * 
     * @param search_key um termo de pesquisa.
     * @return Uma lista de <code>Book</code> com os resultados da pesquisa.
     */
    public static List<Book> doTitleSearch(String search_key) {
        return Bookstore.getBooksByTitle(search_key);
    }

    /**
     * Método estático para pesquisar por author.
     * 
     * @param search_key um termo de pesquisa.
     * @return Uma lista de <code>Book</code> com os resultados da pesquisa.
     */
    public static List<Book> doAuthorSearch(String search_key) {
        return Bookstore.getBooksByAuthor(search_key);
    }

    /**
     * Método estático para retornar novo produtos da plataforma.
     * 
     * @param subject o subject de interesse.
     * @return Uma lista de <code>Book</code> com os novos produtos.
     */
    public static List<Book> getNewProducts(String subject) {
        return Bookstore.getNewBooks(subject);
    }

    /**
     * Método estático para retornar o cost de um livro no e-commerce.
     * 
     * @param book um <code>Book</code> qualquer.
     * @return Uma lista com os preço do parâmetro book em cada store.
     */
    public static List<Double> getCosts(Book book) {
        return getBookstoreStream().map(store -> store.getStock(book.getId())).map(stock -> stock.getCost())
                .collect(Collectors.toList());
    }

    /**
     * Método estático para retornar os best sellers do <code>Bookmarket</code>.
     * 
     * <p>Este método mapeia os bestSellers de cada <code>Bookstore</code>, ordena pelos vendas e retorna uma lista.</p>
     * <p>Se (quantidade menor que 1) ou (quantidade maior 100), retorna 100 livros.</p>
     * 
     * @param quantidade a quantidade a ser mostrada. Deve ser de 0 a 100
     * @return Uma lista de <code>Book</code> com tamanho igual ao parâmetro.
     */
    public static List<Book> getBestSellers(int quantidade) {
    	if (quantidade < 1 || quantidade > 100) {
			quantidade = 100;
		}
    	
    	Stream<Map.Entry<Book, Integer>> bookStream = getBookstoreStream()
                // Set<Entry<Book, Integer>>
                .map(store -> store.getBestSellers().entrySet())
                // Stream<Map.Entry<Book, Integer>>
                .flatMap(entry -> entry.stream());
    	

        return bookStream.sorted((Map.Entry<Book, Integer> e1, Map.Entry<Book, Integer> e2) -> {
        	int result = e2.getValue() - e1.getValue();
			if (result == 0) {
				result = e1.getKey().getTitle().compareTo(e2.getKey().getTitle());
			}
			return result;
        }).limit(quantidade).map(Entry::getKey).collect(Collectors.toList());
    }

    /**
     * Método estático para retornar recomendações de <code>Book</code> similares
     * para um <code>Customer</code>.
     *
     * @param c_id o id do <code>Customer</code>.
     * @return Uma lista de <code>Book</code> de recomendações.
     */
    public static List<Book> getRecommendationByItens(int c_id) {
        return Bookstore.getRecommendationByItens(c_id);
    }

    /**
     * Método estático para retornar recomendações de <code>Book</code> que outros
     * <code>Customer</code> recomendam para um <code>Customer</code>.
     *
     * @param c_id o id do <code>Customer</code>.
     * @return Uma lista de <code>Book</code> de recomendações.
     */
    public static List<Book> getRecommendationByUsers(int c_id) {
        return Bookstore.getRecommendationByUsers(c_id);
    }

    /**
     * Método estático para retornar o estoque um <code>Book</code> em todas as
     * livrarias da plataforma.
     * 
     * @param idBook o id de um <code>Book</code>.
     * @return Uma lista de objetos <code>Stock</code> para um determinado
     *         <code>Book</code> na plataforma.
     */
    public static List<Stock> getStocks(final int idBook) {
        return getBookstoreStream().filter(store -> store.getStock(idBook) != null)
                // transforma o stream de bookstore em stream de stock
                .map(store -> store.getStock(idBook)).collect(Collectors.toList());
    }

    /**
     * Método estático para retornar o estoque um <code>Book</code> em um
     * determinada livraria.
     * 
     * @param idBookstore o id de um <code>Bookstore</code>.
     * @param idBook      o id de um <code>Book</code>.
     * @return O objeto <code>Stock</code> retornado pela busca.
     */
    public static Stock getStock(final int idBookstore, final int idBook) {
        return getBookstoreStream().filter(store -> store.getId() == idBookstore)
                // transforma o stream de bookstore em stream de stock
                .map(store -> store.getStock(idBook)).findFirst().get();
    }

    /**
     * Método estático para retornar os Books relacionados à um <code>Book</code>.
     * 
     * @param i_id o id de um <code>Book</code>.
     * @return Uma lista de <code>Book</code> relacionados ao Book do parâmetro i_id
     */
    public static List<Book> getRelated(int i_id) {
        Book book = Bookstore.getBook(i_id).get();
        ArrayList<Book> related = new ArrayList<>(5);
        related.add(book.getRelated1());
        related.add(book.getRelated2());
        related.add(book.getRelated3());
        related.add(book.getRelated4());
        related.add(book.getRelated5());
        return related;
    }

    /**
     * Método estático para o administrador do e-commerce atualizar um item.
     * 
     * @param iId       o id do item.
     * @param cost      o cost do item.
     * @param image     a image do item.
     * @param thumbnail o thumbnail do item.
     */
    public static void adminUpdate(int iId, double cost, String image, String thumbnail) {
        try {
            stateMachine.execute(new UpdateBookAction(iId, cost, image, thumbnail, System.currentTimeMillis()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Método estático para criar um <code>Cart</code> vazio.
     * 
     * @param storeId o id da loja.
     * @return O id do <code>Cart</code> vazio criado.
     */
    public static int createEmptyCart(int storeId) {
        try {
            return ((Cart) stateMachine.execute(new CreateCartAction(storeId, System.currentTimeMillis()))).getId();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Método estático para atualizar um <code>Cart</code>. Caso o <code>Cart</code>
     * está vazio, retorna um novo <code>Cart</code> com um item qualquer.
     * 
     * @param storeId     o id do <code>Bookstore</code>.
     * @param SHOPPING_ID o id do <code>Cart</code> atual.
     * @param I_ID        o id do item.
     * @param ids         os ids de items.
     * @param quantities  a quantidade de cada item a ser atualizado.
     * @return Um <code>Cart</code> atualizado.
     */
    public static Cart doCart(int storeId, int SHOPPING_ID, Integer I_ID, List<Integer> ids, List<Integer> quantities) {
        try {
            Cart cart = (Cart) stateMachine.execute(
                    new CartUpdateAction(storeId, SHOPPING_ID, I_ID, ids, quantities, System.currentTimeMillis()));
            if (cart.getLines().isEmpty()) {
                Book book = Bookstore.getABookAnyBook(random);
                cart = (Cart) stateMachine.execute(new CartUpdateAction(storeId, SHOPPING_ID, book.getId(),
                        new ArrayList<>(), new ArrayList<>(), System.currentTimeMillis()));
            }
            return cart;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Método para retornar o <code>Cart</code> de um store.
     * 
     * @param SHOPPING_ID o id da compra atual (<code>Cart</code>).
     * @param storeId     o id do <code>Bookstore</code>.
     * @return O objeto <code>Cart</code>.
     */
    public static Cart getCart(int storeId, int SHOPPING_ID) {
        Bookstore bookstore = getBookstoreStream().filter(store -> store.getId() == storeId).findFirst().get();
        synchronized (bookstore) {
            return bookstore.getCart(SHOPPING_ID);
        }
    }

    /**
     * Método estático para realizar uma compra. Não é passado o endereço para
     * envio.
     *
     * @param storeId     o id do <code>Bookstore</code>.
     * @param shopping_id o id do shopping (<code>Cart</code>).
     * @param customer_id o id do customer.
     * @param cc_type     o tipo do cartão de crédito.
     * @param cc_number   o número do cartão de crédito.
     * @param cc_name     o nome do titular do cartão de crédito.
     * @param cc_expiry   a data de expiração do cartão de crédito.
     * @param shipping    o tipo de shipping.
     * @return O objeto <code>Order</code> realizado pela compra.
     */
    public static Order doBuyConfirm(int storeId, int shopping_id, int customer_id, String cc_type, long cc_number,
            String cc_name, Date cc_expiry, String shipping) {
        long now = System.currentTimeMillis();
        try {
            return (Order) stateMachine.execute(new ConfirmBuyAction(storeId, customer_id, shopping_id, randomComment(),
                    cc_type, cc_number, cc_name, cc_expiry, shipping, randomShippingDate(now), -1, now));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Método estático para realizar uma compra. É passado o endereço de entrega.
     *
     * @param storeId     o id do <code>Bookstore</code>.
     * @param shopping_id o id do shopping (<code>Cart</code>).
     * @param customer_id o id do customer.
     * @param cc_type     o tipo do cartão de crédito.
     * @param cc_number   o número do cartão de crédito.
     * @param cc_name     o nome do titular do cartão de crédito.
     * @param cc_expiry   a data de expiração do cartão de crédito.
     * @param shipping    o tipo de shipping.
     * @param street_1    o primeiro valor para o endereço.
     * @param street_2    o primeiro valor para o endereço.
     * @param city        a cidade do endereço.
     * @param state       o estado do endereço.
     * @param zip         o zipcode do endereço.
     * @param country     o país do endereço.
     * @return O objeto <code>Order</code> realizado pela compra.
     */
    public static Order doBuyConfirm(int storeId, int shopping_id, int customer_id, String cc_type, long cc_number,
            String cc_name, Date cc_expiry, String shipping, String street_1, String street_2, String city,
            String state, String zip, String country) {
        Address address = Bookstore.alwaysGetAddress(street_1, street_2, city, state, zip, country);
        long now = System.currentTimeMillis();
        try {
            return (Order) stateMachine.execute(new ConfirmBuyAction(storeId, customer_id, shopping_id, randomComment(),
                    cc_type, cc_number, cc_name, cc_expiry, shipping, randomShippingDate(now), address.getId(), now));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static String randomComment() {
        return TPCW_Util.getRandomString(random, 20, 100);
    }

    private static Date randomShippingDate(long now) {
        return new Date(now + 86400000 /* a day */ * (random.nextInt(7) + 1));
    }

    /**
     * Método estático para popular a banco de dados da aplicação.
     *
     * @param items     a quantidade de itens da plataforma de e-commerce.
     * @param customers a quantidade de customers da plataforma de e-commerce.
     * @param addresses a quantidade de addresses da plataforma de e-commerce.
     * @param authors   a quantidade de authors da plataforma de e-commerce.
     * @param orders    a quantidade de orders da plataforma de e-commerce.
     * @return true se foi populado com sucesso.
     */
    public static boolean populate(int items, int customers, int addresses, int authors, int orders) {
        try {
            return (Boolean) stateMachine.execute(new PopulateAction(random.nextLong(), System.currentTimeMillis(),
                    items, customers, addresses, authors, orders));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Método estático para sinalizar um checkpoint
     */
    public static void checkpoint() {
        try {
            stateMachine.checkpoint();
        } catch (UmbrellaException e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * Método estático para um cliente forneceder uma nota para um livro.
     * 
     * @param customerId o id do <code>Customer</code>.
     * @param bookId o id do <code>Book</code>.
     * @param rating a nota para o item.
     * @return o objeto <code>BookRating</code> criado.
     */
    public static BookRating rateABook(int customerId, int bookId, double rating) {
    	try {
            return (BookRating) stateMachine.execute(new RateABookAction(customerId, bookId, rating));
        } catch (UmbrellaException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * BookstoreAction - Esta classe representa uma evento que será enviado para um
     * <code>Bookstore</code>.
     *
     * Classe abstrata que implementa a interface <code>Action</code>.
     * <br>
	 * *<img src="./doc-files/BookstoreAction.png" alt="BookstoreAction">
     */
    protected static abstract class BookstoreAction implements Action<Stream<Bookstore>>, Serializable {

        /**
         * Implementação do método <code>executeOn()</code> da interface
         * <code>Action</code>.
         *
         * @param sm o <code>Stream</code> de <code>Bookstore</code>.
         * @return Uma resposta de algum evento.
         */
        @Override
        public Object executeOn(Stream<Bookstore> sm) {
            return executeOnBookstore(sm);
        }

        /**
         * Método abstrato que executa um evento em um <code>Bookstore</code>.
         *
         * @param bookstore o <code>Stream</code> de <code>Bookstore</code>.
         * @return Uma resposta de algum evento.
         */
        public abstract Object executeOnBookstore(Stream<Bookstore> bookstore);
    }

    /**
     * CreateCustomerAction - Esta classe encapsula um evento de criar um
     * {@link Customer}.
     * <br>
	 * *<img src="./doc-files/CreateCustomerAction.png" alt="CreateCustomerAction">
     */
    protected static class CreateCustomerAction extends BookstoreAction {

        private static final long serialVersionUID = 6039962163348790677L;

        String fname;
        String lname;
        String street1;
        String street2;
        String city;
        String state;
        String zip;
        String countryName;
        String phone;
        String email;
        double discount;
        Date birthdate;
        String data;
        long now;

        /**
         * Construtor da classe.
         *
         * @param fname       o nome do <code>Customer</code>.
         * @param lname       o sobrenome do <code>Customer</code>.
         * @param street1     o endereço 1 do <code>Customer</code>.
         * @param street2     o endereço 2 do <code>Customer</code>.
         * @param city        a cidade do <code>Customer</code>.
         * @param state       o Estado do <code>Customer</code>.
         * @param zip         o CEP do <code>Customer</code>.
         * @param countryName o nome do País do <code>Customer</code>.
         * @param phone       o telefone do <code>Customer</code>.
         * @param email       o email do <code>Customer</code>.
         * @param discount    o valor do desconto para o <code>Customer</code>.
         * @param birthdate   a data de nascimento do <code>Customer</code>.
         * @param data        informações adicionais sobre o <code>Customer</code>.
         * @param now         o timestamp de criação.
         */
        public CreateCustomerAction(String fname, String lname, String street1, String street2, String city,
                String state, String zip, String countryName, String phone, String email, double discount,
                Date birthdate, String data, long now) {
            this.fname = fname;
            this.lname = lname;
            this.street1 = street1;
            this.street2 = street2;
            this.city = city;
            this.state = state;
            this.zip = zip;
            this.countryName = countryName;
            this.phone = phone;
            this.email = email;
            this.discount = discount;
            this.birthdate = birthdate;
            this.data = data;
            this.now = now;
        }

        /**
         * Método que executa o método <code>createCustomer</code> de
         * <code>Bookstore</code>.
         * <p>
         * Implementação do método <code>executeOnBookstore()</code> de
         * {@link BookstoreAction}.
         * </p>
         *
         * @param bookstore o <code>Stream</code> de <code>Bookstore</code>.
         * @return O objeto <code>Customer</code> criado com sucesso.
         */
        @Override
        public Object executeOnBookstore(Stream<Bookstore> bookstore) {
            return Bookstore.createCustomer(fname, lname, street1, street2, city, state, zip, countryName, phone, email,
                    discount, birthdate, data, now);
        }
    }

    /**
     * RefreshCustomerSessionAction - Esta classe encapsula um evento de atualizar
     * uma sessão na aplicação.
     * <br>
	 * *<img src="./doc-files/RefreshCustomerSessionAction.png" alt="RefreshCustomerSessionAction">
     */
    protected static class RefreshCustomerSessionAction extends BookstoreAction {

        private static final long serialVersionUID = -5391031909452321478L;

        int cId;
        long now;

        /**
         * Construtor da classe.
         *
         * @param id  o id do <code>Customer</code>.
         * @param now o timestamp de agora.
         */
        public RefreshCustomerSessionAction(int id, long now) {
            cId = id;
            this.now = now;
        }

        /**
         * Método que executa o método <code>refreshCustomerSession</code> de
         * <code>Bookstore</code>.
         * <p>
         * Implementação do método <code>executeOnBookstore()</code> de
         * {@link BookstoreAction}.
         * </p>
         *
         * @param bookstore o <code>Stream</code> de <code>Bookstore</code>.
         * @return null.
         */
        @Override
        public Object executeOnBookstore(Stream<Bookstore> bookstore) {
            Bookstore.refreshCustomerSession(cId, now);
            return null;
        }
    }

    /**
     * UpdateBookAction - Esta classe encapsula um evento de atualização de um
     * <code>Book</code>.
     * <br>
	 * *<img src="./doc-files/UpdateBookAction.png" alt="UpdateBookAction">
     */
    protected static class UpdateBookAction extends BookstoreAction {

        private static final long serialVersionUID = -745697943594643776L;

        int bId;
        double cost;
        String image;
        String thumbnail;
        long now;

        /**
         * Construtor da classe.
         *
         * @param id        o id do <code>Book</code>.
         * @param cost      o cost do <code>Book</code>.
         * @param image     a image do <code>Book</code>.
         * @param thumbnail a thumbnail do <code>Book</code>.
         * @param now       o timestamp da atualização do <code>Book</code>.
         */
        public UpdateBookAction(int id, double cost, String image, String thumbnail, long now) {
            bId = id;
            this.cost = cost;
            this.image = image;
            this.thumbnail = thumbnail;
            this.now = now;
        }

        /**
         * Método que executa o método <code>updateBook</code> de
         * <code>Bookstore</code>.
         * <p>
         * Implementação do método <code>executeOnBookstore()</code> de
         * {@link BookstoreAction}.
         * </p>
         *
         * @param bookstore o <code>Stream</code> de <code>Bookstore</code>.
         * @return null.
         */
        @Override
        public Object executeOnBookstore(Stream<Bookstore> bookstore) {
            Bookstore.updateBook(bId, image, thumbnail, now);
            return null;
        }
    }

    /**
     * CreateCartAction - Esta classe encapsula um evento de criação de um
     * <code>Cart</code>.
     * <br>
	 * *<img src="./doc-files/CreateCartAction.png" alt="CreateCartAction">
     */
    protected static class CreateCartAction extends BookstoreAction {

        private static final long serialVersionUID = 8255648428785854052L;

        long now, storeId;

        /**
         * Construtor da classe.
         *
         * @param idBookstore o id da <code>Bookstore</code>.
         * @param now         o timestamp da criação.
         */
        public CreateCartAction(int idBookstore, long now) {
            this.now = now;
            this.storeId = idBookstore;
        }

        /**
         * Método que executa o método <code>createCart</code> de
         * <code>Bookstore</code>.
         * <p>
         * Implementação do método <code>executeOnBookstore()</code> de
         * {@link BookstoreAction}.
         * </p>
         *
         * @param bookstore o <code>Stream</code> de <code>Bookstore</code>.
         * @return O objeto <code>Cart</code> criado.
         */
        @Override
        public Object executeOnBookstore(Stream<Bookstore> bookstore) {
            return bookstore.filter(bs -> bs.getId() == this.storeId).findFirst().get().createCart(now);
        }
    }

    /**
     * CartUpdateAction - Esta classe encapsula um evento de atualização de um
     * <code>Cart</code>.
     * <br>
	 * *<img src="./doc-files/CartUpdateAction.png" alt="CartUpdateAction">
     */
    protected static class CartUpdateAction extends BookstoreAction {

        private static final long serialVersionUID = -6062032194650262105L;

        final int cId, storeId;
        final Integer bId;
        final List<Integer> bIds;
        final List<Integer> quantities;
        final long now;

        /**
         * Construtor da classe.
         *
         * @param storeId    o id do <code>Bookstore</code>.
         * @param id         o id do <code>Cart</code>.
         * @param id2        o id do <code>Book</code>.
         * @param ids        os ids de cada <code>Book</code>.
         * @param quantities a quantidade de cada id de <code>Book</code>.
         * @param now        o timestamp da atualização.
         */
        public CartUpdateAction(int storeId, int id, Integer id2, List<Integer> ids, List<Integer> quantities,
                long now) {
            this.storeId = storeId;
            cId = id;
            bId = id2;
            bIds = ids;
            this.quantities = quantities;
            this.now = now;
        }

        /**
         * Método que executa o método <code>cartUpdate</code> de
         * <code>Bookstore</code>.
         * <p>
         * Implementação do método <code>executeOnBookstore()</code> de
         * {@link BookstoreAction}.
         * </p>
         *
         * @param bookstore o <code>Stream</code> de <code>Bookstore</code>.
         * @return O objeto <code>Cart</code> atualizado.
         */
        @Override
        public Object executeOnBookstore(Stream<Bookstore> bookstore) {
            return bookstore.filter(bs -> bs.getId() == this.storeId).findFirst().get().cartUpdate(cId, bId, bIds,
                    quantities, now);
        }
    }

    /**
     * ConfirmBuyAction - Esta classe encapsula um evento de confirmação de compra.
     * <br>
	 * *<img src="./doc-files/ConfirmBuyAction.png" alt="ConfirmBuyAction">
     */
    protected static class ConfirmBuyAction extends BookstoreAction {

        private static final long serialVersionUID = -6180290851118139002L;

        final int customerId, storeId, cartId;
        String comment;
        String ccType;
        long ccNumber;
        String ccName;
        Date ccExpiry;
        String shipping;
        Date shippingDate;
        int addressId;
        long now;

        /**
         * Construtor da classe.
         *
         * @param storeId      o id do <code>Bookstore</code>.
         * @param customerId   o id do <code>Customer</code>.
         * @param cartId       o id do <code>Cart</code>.
         * @param comment      o comment da compra.
         * @param ccType       o tipo do cartão de crédito.
         * @param ccNumber     o número do cartão de crédito.
         * @param ccName       o nome do titular do cartão de crédito.
         * @param ccExpiry     a data de expiração do cartão de crédito.
         * @param shipping     o tipo de entrega.
         * @param shippingDate a data para entrega.
         * @param addressId    o id do <code>Address</code>.
         * @param now          o timestamp da compra.
         */
        public ConfirmBuyAction(int storeId, int customerId, int cartId, String comment, String ccType, long ccNumber,
                String ccName, Date ccExpiry, String shipping, Date shippingDate, int addressId, long now) {
            this.storeId = storeId;
            this.customerId = customerId;
            this.cartId = cartId;
            this.comment = comment;
            this.ccType = ccType;
            this.ccNumber = ccNumber;
            this.ccName = ccName;
            this.ccExpiry = ccExpiry;
            this.shipping = shipping;
            this.shippingDate = shippingDate;
            this.addressId = addressId;
            this.now = now;
        }

        /**
         * Método que executa o método <code>confirmBuy</code> de
         * <code>Bookstore</code>.
         * <p>
         * Implementação do método <code>executeOnBookstore()</code> de
         * {@link BookstoreAction}.
         * </p>
         *
         * @param bookstore o <code>Stream</code> de <code>Bookstore</code>.
         * @return O objeto <code>Order</code> criado.
         */
        @Override
        public Object executeOnBookstore(Stream<Bookstore> bookstore) {
            return bookstore.filter(bs -> bs.getId() == this.storeId).findFirst().get().confirmBuy(customerId, cartId,
                    comment, ccType, ccNumber, ccName, ccExpiry, shipping, shippingDate, addressId, now);
        }
    }

    /**
     * PopulateAction - Esta classe encapsula um evento de popular o banco de dados.
     * <br>
	 * *<img src="./doc-files/PopulateAction.png" alt="PopulateAction">
     */
    protected static class PopulateAction extends BookstoreAction {

        private static final long serialVersionUID = -5240430799502573886L;

        long seed;
        long now;
        int items;
        int customers;
        int addresses;
        int authors;
        int orders;

        /**
         * Construtor da classe.
         *
         * @param seed      o seed para auxiliar na população da plataforma de e-commerce.
         * @param now       o timestamp para auxiliar na população da plataforma de e-commerce.
         * @param items     a quantidade de itens da plataforma de e-commerce.
         * @param customers a quantidade de customers da plataforma de e-commerce.
         * @param addresses a quantidade de addresses da plataforma de e-commerce.
         * @param authors   a quantidade de authors da plataforma de e-commerce.
         * @param orders    a quantidade de orders da plataforma de e-commerce.
         */
        public PopulateAction(long seed, long now, int items, int customers, int addresses, int authors, int orders) {
            this.seed = seed;
            this.now = now;
            this.items = items;
            this.customers = customers;
            this.addresses = addresses;
            this.authors = authors;
            this.orders = orders;
        }

        /**
         * Método que executa o método <code>populate</code> de <code>Bookstore</code>.
         * <p>
         * Implementação do método <code>executeOnBookstore()</code> de
         * {@link BookstoreAction}.
         * </p>
         *
         * @param bookstore o <code>Stream</code> de <code>Bookstore</code>.
         * @return true.
         */
        @Override
        public Object executeOnBookstore(Stream<Bookstore> bookstore) {
            Bookstore.populate(seed, now, items, customers, addresses, authors);
            Random rand = new Random(seed);
            bookstore.forEach(instance -> {
            	instance.populateInstanceBookstore(orders, rand, now);
            	Bookstore.populateEvaluation(instance.getOrdersById(), rand);
            });
            Bookstore.generateDataSetFile();
            return true;
        }
    }

    /**
     * RateABookAction - Esta classe encapsula um evento de avaliar um <code>Book</code>.
     * <br>
	 * *<img src="./doc-files/RateABookAction.png" alt="RateABookAction">
     */
    protected static class RateABookAction extends BookstoreAction {

        private static final long serialVersionUID = -259631387961609671L;

        int customerId;
        int bookId;
        double rating;

        /**
         * Construtor da classe.
         * 
         * @param customerId o id do <code>Customer</code>.
         * @param bookId o id do <code>Book</code>.
         * @param rating a nota para o item.
         */
        public RateABookAction(int customerId, int bookId, double rating) {
            this.customerId = customerId;
            this.bookId = bookId;
            this.rating = rating;
        }

        /**
        * Método que executa o método <code>rateABook</code> de
        * <code>Bookstore</code>.
        * <p>
        * Implementação do método <code>executeOnBookstore()</code> de
        * {@link BookstoreAction}.
        * </p>
        *
        * @param bookstore o <code>Stream</code> de <code>Bookstore</code>.
        * @return O objeto <code>BookRating</code> criado.
        */
        @Override
        public Object executeOnBookstore(Stream<Bookstore> bookstore) {
            return Bookstore.rateABook(customerId, bookId, rating);
        }
    }
}
