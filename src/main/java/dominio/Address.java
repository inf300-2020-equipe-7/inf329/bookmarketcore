package dominio;

/* 
 * Address.java - Stores an address.
 *
 ************************************************************************
 *
 * This is part of the the Java TPC-W distribution,
 * written by Harold Cain, Tim Heil, Milo Martin, Eric Weglarz, and Todd
 * Bezenek.  University of Wisconsin - Madison, Computer Sciences
 * Dept. and Dept. of Electrical and Computer Engineering, as a part of
 * Prof. Mikko Lipasti's Fall 1999 ECE 902 course.
 *
 * Copyright (C) 1999, 2000 by Harold Cain, Timothy Heil, Milo Martin, 
 *                             Eric Weglarz, Todd Bezenek.
 * Copyright © 2008 Gustavo Maciel Dias Vieira
 *
 * This source code is distributed "as is" in the hope that it will be
 * useful.  It comes with no warranty, and no author or distributor
 * accepts any responsibility for the consequences of its use.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this code under the following conditions:
 *
 * This code is distributed for non-commercial use only.
 * Please contact the maintainer for restrictions applying to 
 * commercial use of these tools.
 *
 * Permission is granted to anyone to make or distribute copies
 * of this code, either as received or modified, in any
 * medium, provided that all copyright notices, permission and
 * nonwarranty notices are preserved, and that the distributor
 * grants the recipient permission for further redistribution as
 * permitted by this document.
 *
 * Permission is granted to distribute this code in compiled
 * or executable form under the same conditions that apply for
 * source code, provided that either:
 *
 * A. it is accompanied by the corresponding machine-readable
 *    source code,
 * B. it is accompanied by a written offer, with no time limit,
 *    to give anyone a machine-readable copy of the corresponding
 *    source code in return for reimbursement of the cost of
 *    distribution.  This written offer must permit verbatim
 *    duplication by anyone, or
 * C. it is distributed by someone who received only the
 *    executable form, and is accompanied by a copy of the
 *    written offer of source code that they received concurrently.
 *
 * In other words, you are welcome to use, share and improve this codes.
 * You are forbidden to forbid anyone else to use, share and improve what
 * you give them.
 *
 ************************************************************************/
import java.io.Serializable;

/** 
 * Address.java - Classe responsável pelos endereços de um <code>Bookstore</code>.
 *  
 * <p>Esta classe é composta por métodos getters e setters e seu construtor</p>
 * 
 * *<img src="./doc-files/Address.png" alt="Address">
 * <br><a href="./doc-files/Address.html"> code </a>
 */
public class Address implements Serializable {

    private static final long serialVersionUID = 3980790290181121772L;

    private final int id;
    private final String street1;
    private final String street2;
    private final String city;
    private final String state;
    private final String zip;
    private final Country country;

    /**
     * Método de construtor da classe.
     * 
     * @param id - o id de um <code>Address</code>
     * @param street1 -  o endereço 1  de um <code>Address</code>
     * @param street2 - o endereço 2 de um <code>Address</code>
     * @param city - a cidade de um <code>Address</code>
     * @param state - o estado de um <code>Address</code>
     * @param zip - o CEP de um <code>Address</code>
     * @param country - o país de um <code>Address</code>
     */
    public Address(int id, String street1, String street2, String city,
            String state, String zip, Country country) {
        this.id = id;
        this.street1 = street1;
        this.street2 = street2;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.country = country;
    }

    /**
     * Método get id (int) de um <code>Address</code> retornando o id.
     * 
     * @return O valor do atributo id
     */
    public int getId() {
        return id;
    }

    /**
     *Método getStreet1 (string) de um <code>Address</code>
     * 
     * @return O valor do atributo street1
     */
    public String getStreet1() {
        return street1;
    }

    /**
     *Método getStreet2 (string) de um <code>Address</code>
     * 
     * @return O valor do atributo street2
     */
    public String getStreet2() {
        return street2;
    }

    /**
     *Método getCity (string) de um <code>Address</code>
     * 
     * @return O valor do atributo city
     */
    public String getCity() {
        return city;
    }

    /**
     *Método getState (string) de um <code>Address</code>
     * 
     * @return O valor do atributo state
     */
    public String getState() {
        return state;
    }

    /**
     *Método getZip (string) de um <code>Address</code>
     * 
     * @return O valor do atributo zip
     */
    public String getZip() {
        return zip;
    }

    /**
     *Método getCountry (Country) de um <code>Address</code>
     * 
     * @return O valor do atributo country
     */
    public Country getCountry() {
        return country;
    }

    /**
     *Método para comparar objetos <code> Address</code>
     * 
     * @param o Qualquer objeto para ser comparado
     * @return true se o objeto for igual e false se ele for diferente
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof Address) {
            Address address = (Address) o;
            return street1.equals(address.street1)
                    && street2.equals(address.street2)
                    && city.equals(address.city)
                    && state.equals(address.state)
                    && zip.equals(address.zip)
                    && country.equals(address.country);
        }
        return false;
    }

    /**
     * Método para gerar um hashcode
     * @return Retorna um valor inteiro para um hashcode
     */
    @Override
    public int hashCode() {
        return street1.hashCode() + street2.hashCode() + city.hashCode()
                + state.hashCode() + zip.hashCode() + country.hashCode();
    }
}
