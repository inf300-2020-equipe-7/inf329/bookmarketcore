package dominio;

/*
 * Order.java - Order class stores data pertinent to a single order.
 * 
 ************************************************************************
 *
 * This is part of the the Java TPC-W distribution,
 * written by Harold Cain, Tim Heil, Milo Martin, Eric Weglarz, and Todd
 * Bezenek.  University of Wisconsin - Madison, Computer Sciences
 * Dept. and Dept. of Electrical and Computer Engineering, as a part of
 * Prof. Mikko Lipasti's Fall 1999 ECE 902 course.
 *
 * Copyright (C) 1999, 2000 by Harold Cain, Timothy Heil, Milo Martin, 
 *                             Eric Weglarz, Todd Bezenek.
 * Copyright © 2008 Gustavo Maciel Dias Vieira
 *
 * This source code is distributed "as is" in the hope that it will be
 * useful.  It comes with no warranty, and no author or distributor
 * accepts any responsibility for the consequences of its use.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this code under the following conditions:
 *
 * This code is distributed for non-commercial use only.
 * Please contact the maintainer for restrictions applying to 
 * commercial use of these tools.
 *
 * Permission is granted to anyone to make or distribute copies
 * of this code, either as received or modified, in any
 * medium, provided that all copyright notices, permission and
 * nonwarranty notices are preserved, and that the distributor
 * grants the recipient permission for further redistribution as
 * permitted by this document.
 *
 * Permission is granted to distribute this code in compiled
 * or executable form under the same conditions that apply for
 * source code, provided that either:
 *
 * A. it is accompanied by the corresponding machine-readable
 *    source code,
 * B. it is accompanied by a written offer, with no time limit,
 *    to give anyone a machine-readable copy of the corresponding
 *    source code in return for reimbursement of the cost of
 *    distribution.  This written offer must permit verbatim
 *    duplication by anyone, or
 * C. it is distributed by someone who received only the
 *    executable form, and is accompanied by a copy of the
 *    written offer of source code that they received concurrently.
 *
 * In other words, you are welcome to use, share and improve this codes.
 * You are forbidden to forbid anyone else to use, share and improve what
 * you give them.
 *
 ************************************************************************/
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Order.java - classe responsável por representar um pedido no <code>Bookstore</code>.
 * 
 * <p>Esta classe é composta pelos atributos, métodos getters e construtor.</p>
 * <br>
 * *<img src="./doc-files/Order.png" alt="Order">
 * <br><a href="./doc-files/Order.html"> code </a>
 */
public class Order implements Serializable {

    private static final long serialVersionUID = -1106285234830970111L;

    private final int id; //  o id único de um <code>Order</code>
    private final Customer customer; // o Custumer único de <code>Order</code>
    private final Date date; // o Date único de um <code>Order</code>
    private final double subtotal; // o subtotal de um <code>Order</code>
    private final double tax; // o tax de um <code>Order</code>
    private final double total; // o total de um <code>Order</code>
    private final String shipType; // o metodo de envio de um <code>Order</code>
    private final Date shipDate; // a data de envio de um <code>Order</code>
    private final String status; // o status de um <code>Order</code>
    private final Address billingAddress; // o endereço de cobranca de um <code>Order</code>
    private final Address shippingAddress; // o endereço de entrega de um <code>Order</code>
    private final CCTransaction cc; // a trasacao de um de um cartao de credito de um <code>Order</code>
    private final ArrayList<OrderLine> lines; // a lista de produtos de um <code>Order</code>

    /**
     * Método de atribuições de informações e adição de produtos a um array list de um <code>Order</code> 
     * 
     * 
     * <pre>
     * this.id = id;
     * this.customer = customer;
     * this.date = date;
     * subtotal = cart.subTotal(customer.getDiscount());
     * tax = 8.25;
     * total = cart.total(customer.getDiscount());
     * this.shipType = shipType;
     * this.shipDate = shipDate;
     * this.status = status;
     * this.billingAddress = billingAddress;
     * this.shippingAddress = shippingAddress;
     * this.cc = cc;
     * lines = new ArrayList&lt;OrderLine&gt;(cart.getLines().size());
     * for (CartLine cartLine : cart.getLines()) {
     * OrderLine line = new OrderLine(cartLine.getBook(),
     * cartLine.getQty(), customer.getDiscount(),
     * comment);
     * lines.add(line);
     * }
     * </pre>
     *
     * @param id o id para um <code>Order</code>
     * @param customer o custumer de um <code>Order</code>
     * @param date a data de um <code>Order</code>
     * @param cart o carrinho de um <code>Order</code>
     * @param comment o comentário de um <code>Order</code>
     * @param shipType o tipo de envio de produto de um <code>Order</code>
     * @param shipDate a data de envio de um <code>Order</code>
     * @param status o status de um <code>Order</code>
     * @param billingAddress o endereço de cobrança de um <code>Order</code>
     * @param shippingAddress o endereço de entrega de um <code>Order</code>
     * @param cc o cartão de crédito de um <code>Order</code>
     */
    public Order(int id, Customer customer, Date date, Cart cart,
            String comment, String shipType, Date shipDate, String status,
            Address billingAddress, Address shippingAddress, CCTransaction cc) {
        this.id = id;
        this.customer = customer;
        this.date = date;
        subtotal = cart.subTotal(customer.getDiscount());
        tax = 8.25;
        total = cart.total(customer.getDiscount());
        this.shipType = shipType;
        this.shipDate = shipDate;
        this.status = status;
        this.billingAddress = billingAddress;
        this.shippingAddress = shippingAddress;
        this.cc = cc;
        lines = new ArrayList<OrderLine>(cart.getLines().size());
        for (CartLine cartLine : cart.getLines()) {
            OrderLine line = new OrderLine(cartLine.getBook(),
                    cartLine.getQty(), customer.getDiscount(),
                    comment);
            lines.add(line);
        }
    }

    /**
     *Método para receber um id (int) de um <code>Order</code>
     * 
     * @return o <code>id</code> de um <code>Order</code> 
     */
    public int getId() {
        return id;
    }

    /**
     *Método para receber um custumer de um <code>Order</code>
     *
     * @return o <code>custumer</code> de um <code>Order</code>
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     *Método para receber uma data (getDate) de um <code>Order</code>
     * 
     * @return o <code>date</code> de um <code>Order</code>
     */
    public Date getDate() {
        return date;
    }

    /**
     * Método para receber o SubTotal de um <code>Order</code>
     *
     * @return o <code>subtotal</code> de um <code>Order</code>
     */
    public double getSubtotal() {
        return subtotal;
    }

    /**
     * Método para receber o Tax de um <code>Order</code>
     *
     * @return o <code>Order</code> de um <code>Order</code>
     */
    public double getTax() {
        return tax;
    }

    /**
     * Método para receber o Total (getTotal) de um <code>Order</code>
     *
     * @return o <code>total</code> de um <code>Order</code>
     */
    public double getTotal() {
        return total;
    }

    /**
     * Método para recebe o tipo de entrega de um <code>Order</code>
     *
     * @return o <code>shipType</code> de um <code>Order</code>
     */
    public String getShipType() {
        return shipType;
    }

    /**
     * Método para recebe a data de envio de um <code>Order</code>
     *
     * @return o <code>shipDate</code> de um <code>Order</code>
     */
    public Date getShipDate() {
        return shipDate;
    }

    /**
     * Getter do atributo <code>status</code>.
     * 
     * @return o <code>status</code> de um <code>Order</code>.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Getter do atributo <code>billingAddress</code>.
     * 
     * @return o <code>billingAddress</code> de um <code>Order</code>.
     */
    public Address getBillingAddress() {
        return billingAddress;
    }

    /**
     * Getter do atributo <code>shippingAddress</code>.
     * 
     * @return o <code>shippingAddress</code> de um <code>Order</code>.
     */
    public Address getShippingAddress() {
        return shippingAddress;
    }

    /**
     * Getter do atributo <code>cc</code>.
     * 
     * @return o <code>cc</code> de um <code>Order</code>.
     */
    public CCTransaction getCC() {
        return cc;
    }

    /**
     * Getter do atributo <code>lines</code>.
     * 
     * @return o <code>lines</code> de um <code>Order</code>.
     */
    public ArrayList<OrderLine> getLines() {
        return lines;
    }

}
