package dominio;

/* 
 * Customer.java - stores the important information for a single customer. 
 *
 ************************************************************************
 *
 * This is part of the the Java TPC-W distribution,
 * written by Harold Cain, Tim Heil, Milo Martin, Eric Weglarz, and Todd
 * Bezenek.  University of Wisconsin - Madison, Computer Sciences
 * Dept. and Dept. of Electrical and Computer Engineering, as a part of
 * Prof. Mikko Lipasti's Fall 1999 ECE 902 course.
 *
 * Copyright (C) 1999, 2000 by Harold Cain, Timothy Heil, Milo Martin, 
 *                             Eric Weglarz, Todd Bezenek.
 * Copyright © 2008 Gustavo Maciel Dias Vieira
 *
 * This source code is distributed "as is" in the hope that it will be
 * useful.  It comes with no warranty, and no author or distributor
 * accepts any responsibility for the consequences of its use.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this code under the following conditions:
 *
 * This code is distributed for non-commercial use only.
 * Please contact the maintainer for restrictions applying to 
 * commercial use of these tools.
 *
 * Permission is granted to anyone to make or distribute copies
 * of this code, either as received or modified, in any
 * medium, provided that all copyright notices, permission and
 * nonwarranty notices are preserved, and that the distributor
 * grants the recipient permission for further redistribution as
 * permitted by this document.
 *
 * Permission is granted to distribute this code in compiled
 * or executable form under the same conditions that apply for
 * source code, provided that either:
 *
 * A. it is accompanied by the corresponding machine-readable
 *    source code,
 * B. it is accompanied by a written offer, with no time limit,
 *    to give anyone a machine-readable copy of the corresponding
 *    source code in return for reimbursement of the cost of
 *    distribution.  This written offer must permit verbatim
 *    duplication by anyone, or
 * C. it is distributed by someone who received only the
 *    executable form, and is accompanied by a copy of the
 *    written offer of source code that they received concurrently.
 *
 * In other words, you are welcome to use, share and improve this codes.
 * You are forbidden to forbid anyone else to use, share and improve what
 * you give them.
 *
 ************************************************************************/
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Customer.java - classe responsável por representar um cliente.
 * 
 * <p>Esta classe é composta pelos atributos, métodos getters, setters e construtores.</p>
 * <p>Um <code>Customer</code> possuí um {@link Address} e uma referência para seu pedido mais recente {@link Order}</p>
 * <br>
 * *<img src="./doc-files/Customer.png" alt="Customer">
 * <br><a href="./doc-files/Customer.html"> code </a>
 */
public class Customer implements Serializable {

    private static final long serialVersionUID = -7297414189618511748L;

    private final int id; // o id único para um <code>Customer</code>.
    private final String uname; // o username único para um <code>Customer</code>.
    private final String passwd; // o password para um <code>Customer</code>.
    private final String fname; // o primeiro nome para um <code>Customer</code>.
    private final String lname; // o sobrenome para um <code>Customer</code>.
    private final String phone; // o telefone para um <code>Customer</code>.
    private final String email; // o email para um <code>Customer</code>.
    private final Date since; // a data de registro para um <code>Customer</code>.
    private final Date lastVisit; // a data da última visita para um <code>Customer</code>.
    private Date login; // a data do ínicio da sessão atual para um <code>Customer</code>.
    private Date expiration; // a data de expiração da sessão atual para um <code>Customer</code>.
    private final double discount; // a porcentagem de desconto para um <code>Customer</code>.
    private final double balance; // o balanço para um <code>Customer</code>.
    private final double ytdPmt; // o payment ytd para um <code>Customer</code>.
    private final Date birthdate; // a data de nascimento para um <code>Customer</code>.
    private final String data; // informações extras para um <code>Customer</code>.
    private final Address address; // o endereço <code>Address</code> para um <code>Customer</code>.
    private Order mostRecentOrder; // a referência mais recente para um <code>Order</code> para um <code>Customer</code>.

    /**
     * Construtor da classe.
     * 
     * @param id o id único para um <code>Customer</code>.
     * @param uname o username único para um <code>Customer</code>.
     * @param passwd o password para um <code>Customer</code>.
     * @param fname o primeiro nome para um <code>Customer</code>.
     * @param lname o sobrenome para um <code>Customer</code>.
     * @param phone o telefone para um <code>Customer</code>.
     * @param email o email para um <code>Customer</code>.
     * @param since a data de registro para um <code>Customer</code>.
     * @param lastVisit a data da última visita para um <code>Customer</code>.
     * @param login a data do ínicio da sessão atual para um <code>Customer</code>.
     * @param expiration a data de expiração da sessão atual para um <code>Customer</code>.
     * @param discount a porcentagem de desconto para um <code>Customer</code>.
     * @param balance o balanço para um <code>Customer</code>.
     * @param ytdPmt o payment ytd para um <code>Customer</code>.
     * @param birthdate a data de nascimento para um <code>Customer</code>.
     * @param data informações extras para um <code>Customer</code>.
     * @param address o endereço <code>Address</code> para um <code>Customer</code>.
     */
    public Customer(int id, String uname, String passwd, String fname,
            String lname, String phone, String email, Date since,
            Date lastVisit, Date login, Date expiration, double discount,
            double balance, double ytdPmt, Date birthdate, String data,
            Address address) {
        this.id = id;
        this.uname = uname;
        this.passwd = passwd;
        this.fname = fname;
        this.lname = lname;
        this.phone = phone;
        this.email = email;
        this.since = since;
        this.lastVisit = lastVisit;
        this.login = login;
        this.expiration = expiration;
        this.discount = discount;
        this.balance = balance;
        this.ytdPmt = ytdPmt;
        this.birthdate = birthdate;
        this.data = data;
        this.address = address;
        mostRecentOrder = null;
    }

    /**
     * Construtor da classe.
     * 
     * @param id o id único para um <code>Customer</code>.
     * @param uname o username único para um <code>Customer</code>.
     * @param toLowerCase definir se é para transformar para lowercase.
     * @param fname o primeiro nome para um <code>Customer</code>.
     * @param lname o sobrenome para um <code>Customer</code>.
     * @param phone o telefone para um <code>Customer</code>.
     * @param email o email para um <code>Customer</code>.
     * @param since a data de registro para um <code>Customer</code>.
     * @param lastVisit a data da última visita para um <code>Customer</code>.
     */
    public Customer(int id, String uname, String toLowerCase, String fname, String lname, String phone, String email, Date since, Date lastVisit) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Setter do atributo <code>login</code>.
     * 
     * @param login a data do ínicio da sessão atual.
     */
    public void setLogin(Date login) {
        this.login = login;
    }

    /**
     * Setter do atributo <code>expiration</code>.
     * 
     * @param expiration a data de expiração da sessão atual.
     */
    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    /**
     * Getter do atributo <code>fname</code>.
     * 
     * @return o <code>fname</code> de um <code>Customer</code>.
     */
    public String getFname() {
        return fname;
    }

    /**
     * Getter do atributo <code>lname</code>.
     * 
     * @return o <code>lname</code> de um <code>Customer</code>.
     */
    public String getLname() {
        return lname;
    }

    /**
     * Getter do atributo <code>id</code>.
     * 
     * @return o <code>id</code> de um <code>Customer</code>.
     */
    public int getId() {
        return id;
    }

    /**
     * Getter do atributo <code>passwd</code>.
     * 
     * @return o <code>passwd</code> de um <code>Customer</code>.
     */
    public String getPasswd() {
        return passwd;
    }

    /**
     * Getter do atributo <code>discount</code>.
     * 
     * @return o <code>discount</code> de um <code>Customer</code>.
     */
    public double getDiscount() {
        return discount;
    }

    /**
     * Getter do atributo <code>address</code>.
     * 
     * @return o <code>address</code> de um <code>Customer</code>.
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Getter do atributo <code>phone</code>.
     * 
     * @return o <code>phone</code> de um <code>Customer</code>.
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Getter do atributo <code>email</code>.
     * 
     * @return o <code>email</code> de um <code>Customer</code>.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Getter do atributo <code>uname</code>.
     * 
     * @return o <code>uname</code> de um <code>Customer</code>.
     */
    public String getUname() {
        return uname;
    }

    /**
     * Setter do atributo <code>order</code>.
     * 
     * @param order a referência mais recente para um <code>Order</code>.
     */
    public void logOrder(Order order) {
        mostRecentOrder = order;
    }

    /**
     * Getter do atributo <code>mostRecentOrder</code>.
     * 
     * @return o <code>mostRecentOrder</code> de um <code>Customer</code>.
     */
    public Order getMostRecentOrder() {
        return mostRecentOrder;
    }

    /**
     * Getter do atributo <code>since</code>.
     * 
     * @return o <code>since</code> de um <code>Customer</code>.
     */
    public Date getSince() {
        return since;
    }

    /**
     * Getter do atributo <code>lastVisit</code>.
     * 
     * @return o <code>lastVisit</code> de um <code>Customer</code>.
     */
    public Date getLastVisit() {
        return lastVisit;
    }

    /**
     * Getter do atributo <code>login</code>.
     * 
     * @return o <code>login</code> de um <code>Customer</code>.
     */
    public Date getLogin() {
        return login;
    }

    /**
     * Getter do atributo <code>expiration</code>.
     * 
     * @return o <code>expiration</code> de um <code>Customer</code>.
     */
    public Date getExpiration() {
        return expiration;
    }

    /**
     * Getter do atributo <code>balance</code>.
     * 
     * @return o <code>balance</code> de um <code>Customer</code>.
     */
    public double getBalance() {
        return balance;
    }

    /**
     * Getter do atributo <code>ytdPmt</code>.
     * 
     * @return o <code>ytdPmt</code> de um <code>Customer</code>.
     */
    public double getYtdPmt() {
        return ytdPmt;
    }

    /**
     * Getter do atributo <code>birthdate</code>.
     * 
     * @return o <code>birthdate</code> de um <code>Customer</code>.
     */
    public Date getBirthdate() {
        return birthdate;
    }

    /**
     * Getter do atributo <code>data</code>.
     * 
     * @return o <code>data</code> de um <code>Customer</code>.
     */
    public String getData() {
        return data;
    }

    /**
     * Método para gerar um hashCode para um <code>Customer</code>.
     * 
     * @return um valor inteiro.
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + this.id;
        hash = 29 * hash + Objects.hashCode(this.uname);
        hash = 29 * hash + Objects.hashCode(this.passwd);
        hash = 29 * hash + Objects.hashCode(this.fname);
        hash = 29 * hash + Objects.hashCode(this.lname);
        hash = 29 * hash + Objects.hashCode(this.phone);
        hash = 29 * hash + Objects.hashCode(this.email);
        hash = 29 * hash + Objects.hashCode(this.since);
        hash = 29 * hash + Objects.hashCode(this.lastVisit);
        hash = 29 * hash + Objects.hashCode(this.login);
        hash = 29 * hash + Objects.hashCode(this.expiration);
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.discount) ^ (Double.doubleToLongBits(this.discount) >>> 32));
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.balance) ^ (Double.doubleToLongBits(this.balance) >>> 32));
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.ytdPmt) ^ (Double.doubleToLongBits(this.ytdPmt) >>> 32));
        hash = 29 * hash + Objects.hashCode(this.birthdate);
        hash = 29 * hash + Objects.hashCode(this.data);
        hash = 29 * hash + Objects.hashCode(this.address);
        hash = 29 * hash + Objects.hashCode(this.mostRecentOrder);
        return hash;
    }

    /**
     * Método para comparar objetos <code>Customer</code>.
     * 
     * @param obj um objeto qualquer para ser comparado.
     * @return true se os objetos são iguais. false se são diferentes.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Customer other = (Customer) obj;
        if (this.id != other.id) {
            return false;
        }
        if (Double.doubleToLongBits(this.discount) != Double.doubleToLongBits(other.discount)) {
            return false;
        }
        if (Double.doubleToLongBits(this.balance) != Double.doubleToLongBits(other.balance)) {
            return false;
        }
        if (Double.doubleToLongBits(this.ytdPmt) != Double.doubleToLongBits(other.ytdPmt)) {
            return false;
        }
        if (!Objects.equals(this.uname, other.uname)) {
            return false;
        }
        if (!Objects.equals(this.passwd, other.passwd)) {
            return false;
        }
        if (!Objects.equals(this.fname, other.fname)) {
            return false;
        }
        if (!Objects.equals(this.lname, other.lname)) {
            return false;
        }
        if (!Objects.equals(this.phone, other.phone)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.data, other.data)) {
            return false;
        }
        if (!Objects.equals(this.since, other.since)) {
            return false;
        }
        if (!Objects.equals(this.lastVisit, other.lastVisit)) {
            return false;
        }
        if (!Objects.equals(this.login, other.login)) {
            return false;
        }
        if (!Objects.equals(this.expiration, other.expiration)) {
            return false;
        }
        if (!Objects.equals(this.birthdate, other.birthdate)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.mostRecentOrder, other.mostRecentOrder)) {
            return false;
        }
        return true;
    }

   



}
