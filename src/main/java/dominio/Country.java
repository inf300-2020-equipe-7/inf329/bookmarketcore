package dominio;

/* 
 * Address.java - Stores a country.
 *
 ************************************************************************
 *
 * This is part of the the Java TPC-W distribution,
 * written by Harold Cain, Tim Heil, Milo Martin, Eric Weglarz, and Todd
 * Bezenek.  University of Wisconsin - Madison, Computer Sciences
 * Dept. and Dept. of Electrical and Computer Engineering, as a part of
 * Prof. Mikko Lipasti's Fall 1999 ECE 902 course.
 *
 * Copyright (C) 1999, 2000 by Harold Cain, Timothy Heil, Milo Martin, 
 *                             Eric Weglarz, Todd Bezenek.
 * Copyright © 2008 Gustavo Maciel Dias Vieira
 *
 * This source code is distributed "as is" in the hope that it will be
 * useful.  It comes with no warranty, and no author or distributor
 * accepts any responsibility for the consequences of its use.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this code under the following conditions:
 *
 * This code is distributed for non-commercial use only.
 * Please contact the maintainer for restrictions applying to 
 * commercial use of these tools.
 *
 * Permission is granted to anyone to make or distribute copies
 * of this code, either as received or modified, in any
 * medium, provided that all copyright notices, permission and
 * nonwarranty notices are preserved, and that the distributor
 * grants the recipient permission for further redistribution as
 * permitted by this document.
 *
 * Permission is granted to distribute this code in compiled
 * or executable form under the same conditions that apply for
 * source code, provided that either:
 *
 * A. it is accompanied by the corresponding machine-readable
 *    source code,
 * B. it is accompanied by a written offer, with no time limit,
 *    to give anyone a machine-readable copy of the corresponding
 *    source code in return for reimbursement of the cost of
 *    distribution.  This written offer must permit verbatim
 *    duplication by anyone, or
 * C. it is distributed by someone who received only the
 *    executable form, and is accompanied by a copy of the
 *    written offer of source code that they received concurrently.
 *
 * In other words, you are welcome to use, share and improve this codes.
 * You are forbidden to forbid anyone else to use, share and improve what
 * you give them.
 *
 ************************************************************************/
import java.io.Serializable;

/**
 * Country.java - classe responsável por representar um país.
 * 
 * <p>Esta classe é composta pelos atributos, métodos getters e um construtor.</p>
 * <br>
 * *<img src="./doc-files/Country.png" alt="Country">
 * <br><a href="./doc-files/Country.html"> code </a>
 */
public class Country implements Serializable {

    private static final long serialVersionUID = 5171617014956861344L;

    private final int id; // o id único para um <code>Country</code>
    private final String name; // o nome para um <code>Country</code>
    private final String currency; // o nome da moeda para um <code>Country</code>
    private final double exchange; // a taxa de câmbio para um <code>Country</code>

    /**
     * Construtor da classe.
     * 
     * @param id o id único para um <code>Country</code>
     * @param name o nome para um <code>Country</code>
     * @param currency o nome da moeda para um <code>Country</code>
     * @param exchange a taxa de câmbio para um <code>Country</code>
     */
    public Country(int id, String name, String currency, double exchange) {
        this.id = id;
        this.name = name;
        this.currency = currency;
        this.exchange = exchange;
    }

    /**
     * Getter do atributo <code>id</code>.
     * 
     * @return o <code>id</code> de um <code>Country</code>.
     */
    public int getId() {
        return id;
    }

    /**
     * Getter do atributo <code>name</code>.
     * 
     * @return o <code>name</code> de um <code>Country</code>.
     */
    public String getName() {
        return name;
    }

    /**
     * Getter do atributo <code>currency</code>.
     * 
     * @return o <code>currency</code> de um <code>Country</code>.
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Getter do atributo <code>exchange</code>.
     * 
     * @return o <code>exchange</code> de um <code>Country</code>.
     */
    public double getExchange() {
        return exchange;
    }

    /**
     * Método para comparar objetos <code>Country</code>.
     * 
     * @param o um objeto qualquer para ser comparado.
     * @return true se os objetos são iguais. false se são diferentes.
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof Country) {
            Country country = (Country) o;
            return name.equals(country.name);
        }
        return false;
    }

    /**
     * Método para gerar um hashCode para um <code>Country</code>.
     * 
     * @return um valor inteiro.
     */
    @Override
    public int hashCode() {
        return name.hashCode();
    }

}
