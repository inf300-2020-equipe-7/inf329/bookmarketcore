package dominio;

import java.io.Serializable;

/**
 * BookRating.java - Classe responsável pelas avaliações de um <code>Customer</code> para um <code>Book</code>.
 *  
 * <p>Esta classe é composta por métodos getters e setters e seu construtor.</p>
 * <br>
 * *<img src="./doc-files/BookRating.png" alt="BookRating">
 * <br><a href="./doc-files/BookRating.html"> code </a>
 */
public class BookRating implements Serializable {
	private static final long serialVersionUID = 4789320996668173749L;
	
	private final int id; // o id único para um <code>BookRating</code>
    private final Customer customer; // o <code>Customer</code> associado ao <code>BookRating</code>
    private final Book book; // o <code>Book</code> associado ao <code>BookRating</code>
    private double rating; // a nota de avaliação para um <code>BookRating</code>
    
    /**
     * Construtor da classe.
     * @param id o id único para um <code>BookRating</code>
     * @param customer o <code>Customer</code> associado ao <code>BookRating</code>
     * @param book o <code>Book</code> associado ao <code>BookRating</code>
     * @param rating a nota de avaliação para um <code>BookRating</code>
     */
    public BookRating(int id, Customer customer, Book book, double rating) {
        this.id = id;
        this.customer = customer;
        this.book = book;
        this.rating = rating;
    }

    /**
     * Getter do atributo <code>id</code>.
	 * 
     * @return o <code>id</code> de um <code>BookRating</code>.
     */
    public int getId() {
        return this.id;
    }
    
    /**
     * Getter do atributo <code>customer</code>.
	 * 
     * @return o <code>customer</code> de um <code>BookRating</code>.
     */
    public Customer getCustomer() {
        return this.customer;
    }

    /**
     * Getter do atributo <code>book</code>.
	 * 
     * @return o <code>book</code> de um <code>BookRating</code>.
     */
    public Book getBook() {
        return this.book;
    }

    /**
     * Getter do atributo <code>rating</code>.
	 * 
     * @return o <code>rating</code> de um <code>BookRating</code>.
     */
    public double getRating() {
        return this.rating;
    }
    
    /**
     * Setter do atributo <code>rating</code>.
	 * 
     * @param rate o <code>rate</code> de um <code>BookRating</code>.
     */
    public void setRating(double rate){
        this.rating = rate;
    }

    /**
     * Método para gerar um hashCode para um <code>BookRating</code>.
     * 
     * @return um valor inteiro.
     */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result + ((customer == null) ? 0 : customer.hashCode());
		result = prime * result + id;
		long temp;
		temp = Double.doubleToLongBits(rating);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/**
     * Método para comparar objetos <code>BookRating</code>.
     * 
     * @param obj um objeto qualquer para ser comparado.
     * @return true se os objetos são iguais. false se são diferentes.
     */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof BookRating)) {
			return false;
		}
		BookRating other = (BookRating) obj;
		if (book == null) {
			if (other.book != null) {
				return false;
			}
		} else if (!book.equals(other.book)) {
			return false;
		}
		if (customer == null) {
			if (other.customer != null) {
				return false;
			}
		} else if (!customer.equals(other.customer)) {
			return false;
		}
		if (id != other.id) {
			return false;
		}
		if (Double.doubleToLongBits(rating) != Double.doubleToLongBits(other.rating)) {
			return false;
		}
		return true;
	}

	/**
	 * Método sobreescrito de <code>toString()</code>.
	 */
	@Override
	public String toString() {
		return "BookRating [id=" + id + ", customer=" + customer + ", book=" + book + ", rating=" + rating + "]";
	}
	
	/**
	 * Método para gerar uma linha para o arquivo de dataset.
	 * 
	 * @return uma String no formato para o arquivo de dataset.
	 */
	public String toStringDataset() {
		return "" + customer.getId() + "," + book.getId() + "," + rating + "\n";
	}
}   