package dominio;

/**
 * Stock.java - classe responsável por representar a relação estoque de um <code>Bookstore</code> e um <code>Book</code>.
 * 
 * <p>Esta classe é composta pelos atributos, métodos getters, setters e construtor.</p>
 * <br>
 * <img src="./doc-files/Stock.png" alt="Bookmarket">
 * <br><a href="./doc-files/Stock.html"> code </a>
 *
 */
public class Stock {
    
    private final int idBookstore; // o id de um <code>Bookstore</code>.
    private final Book book; // o objeto <code>Book</code> relacionado à este <code>Stock</code>.
    private double cost; // o valor do <code>Stock</code>.
    private int qty; // a quantidade do <code>Stock</code>.

    /**
     * Construtor da classe.
     * 
     * @param idBookstore o id de um <code>Bookstore</code>.
     * @param book o objeto <code>Book</code> relacionado à este <code>Stock</code>.
     * @param cost o valor do <code>Stock</code>.
     * @param qty a quantidade do <code>Stock</code>.
     */
    public Stock(int idBookstore, Book book, double cost, int qty) {
        this.idBookstore = idBookstore;
        this.book = book;
        this.cost = cost;
        this.qty = qty;
    }

    /**
     * Getter do atributo <code>book</code>.
     * 
     * @return o <code>book</code> de um <code>Stock</code>.
     */
    public Book getBook() {
        return book;
    }
    
    /**
     * Método para somar um valor ao atributo <code>qty</code>.
     * 
     * @param amount um valor para ser somado ao estoque.
     */
    public void addQty(int amount) {
        qty += amount;
    }

    /**
     * Getter do atributo <code>cost</code>.
     * 
     * @return o <code>cost</code> de um <code>Stock</code>.
     */
    public double getCost() {
        return cost;
    }

    /**
     * Setter do atributo <code>cost</code>.
     * 
     * @param cost o valor do <code>Stock</code>.
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /**
     * Getter do atributo <code>qty</code>.
     * 
     * @return o <code>qty</code> de um <code>Stock</code>.
     */
    public int getQty() {
        return qty;
    }

    /**
     * Setter do atributo <code>qty</code>.
     * 
     * @param qty a quantidade do <code>Stock</code>.
     */
    public void setQty(int qty) {
        this.qty = qty;
    }

    /**
     * Getter do atributo <code>idBookstore</code>.
     * 
     * @return o <code>idBookstore</code> de um <code>Stock</code>.
     */
    public int getIdBookstore() {
        return idBookstore;
    }
    
 
    
}
