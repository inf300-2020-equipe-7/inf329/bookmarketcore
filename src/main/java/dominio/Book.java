package dominio;

/*
 * Book.java - Class used to store all of the data associated with a single
 *             book. 
 * 
 ************************************************************************
 *
 * This is part of the the Java TPC-W distribution,
 * written by Harold Cain, Tim Heil, Milo Martin, Eric Weglarz, and Todd
 * Bezenek.  University of Wisconsin - Madison, Computer Sciences
 * Dept. and Dept. of Electrical and Computer Engineering, as a part of
 * Prof. Mikko Lipasti's Fall 1999 ECE 902 course.
 *
 * Copyright (C) 1999, 2000 by Harold Cain, Timothy Heil, Milo Martin, 
 *                             Eric Weglarz, Todd Bezenek.
 * Copyright © 2008 Gustavo Maciel Dias Vieira
 *
 * This source code is distributed "as is" in the hope that it will be
 * useful.  It comes with no warranty, and no author or distributor
 * accepts any responsibility for the consequences of its use.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this code under the following conditions:
 *
 * This code is distributed for non-commercial use only.
 * Please contact the maintainer for restrictions applying to 
 * commercial use of these tools.
 *
 * Permission is granted to anyone to make or distribute copies
 * of this code, either as received or modified, in any
 * medium, provided that all copyright notices, permission and
 * nonwarranty notices are preserved, and that the distributor
 * grants the recipient permission for further redistribution as
 * permitted by this document.
 *
 * Permission is granted to distribute this code in compiled
 * or executable form under the same conditions that apply for
 * source code, provided that either:
 *
 * A. it is accompanied by the corresponding machine-readable
 *    source code,
 * B. it is accompanied by a written offer, with no time limit,
 *    to give anyone a machine-readable copy of the corresponding
 *    source code in return for reimbursement of the cost of
 *    distribution.  This written offer must permit verbatim
 *    duplication by anyone, or
 * C. it is distributed by someone who received only the
 *    executable form, and is accompanied by a copy of the
 *    written offer of source code that they received concurrently.
 *
 * In other words, you are welcome to use, share and improve this codes.
 * You are forbidden to forbid anyone else to use, share and improve what
 * you give them.
 *
 ************************************************************************/
import java.io.Serializable;
import java.util.Date;

/**
 * Book.java - classe responsável por representar um livro de um <code>Bookstore</code>.
 * 
 * <p>Esta classe é composta pelos atributos, métodos getters e setter e um construtor.</p>
 * <br>
 * *<img src="./doc-files/Book.png" alt="Book">
 * <br><a href="./doc-files/Book.html"> code </a>
 */
public class Book implements Serializable {

    private static final long serialVersionUID = 6505830715531808617L;

    private final int id; // o id de um <code>Book</code>. 
    private final String title; // o título de um <code>Book</code>.
    private Date pubDate; // a data de publicação de um <code>Book</code>.
    private final String publisher; // a editora de um <code>Book</code>.
    private final String subject; // o assunto de um <code>Book</code>.
    private final String desc; // uma descrição de um <code>Book</code>.
    private Book related1; // o primeiro <code>Book</code> relacionado.
    private Book related2; // o segundo <code>Book</code> relacionado.
    private Book related3; // o terceiro <code>Book</code> relacionado.
    private Book related4; // o quarto <code>Book</code> relacionado.
    private Book related5; // o quinto <code>Book</code> relacionado.
    private String thumbnail; // o caminho para uma thumbnail de um <code>Book</code>.
    private String image; // o caminho para uma imagem de um <code>Book</code>.
    private final double srp; // o preço de venda sugerido de um <code>Book</code>.
    private final Date avail; // a data que o livro está disponível de um <code>Book</code>.
    private final String isbn; // o isbn de um <code>Book</code>.
    private final int page; // a quantidade de páginas de um <code>Book</code>.
    private final String backing; // o tipo de um <code>Book</code>.
    private final String dimensions; // as dimensões de um <code>Book</code>.
    private final Author author; // o <code>Author</code> de um <code>Book</code>.

    /**
     * Construto da classe.
     *  
     * @param id o id de um <code>Book</code>. 
     * @param title o título de um <code>Book</code>.
     * @param pubDate data de publicação de um <code>Book</code>.
     * @param publisher a editora de um <code>Book</code>.
     * @param subject o assunto de um <code>Book</code>.
     * @param desc uma descrição de um <code>Book</code>.
     * @param thumbnail o caminho para uma thumbnail de um <code>Book</code>.
     * @param image o caminho para uma imagem de um <code>Book</code>.
     * @param srp o preço de venda sugerido de um <code>Book</code>.
     * @param avail a data que o livro está disponível de um <code>Book</code>.
     * @param isbn o isbn de um <code>Book</code>.
     * @param page a quantidade de páginas de um <code>Book</code>.
     * @param backing o tipo de um <code>Book</code>.
     * @param dimensions as dimensões de um <code>Book</code>.
     * @param author o <code>Author</code> de um <code>Book</code>.
     */
    public Book(int id, String title, Date pubDate, String publisher,
            String subject, String desc, String thumbnail,
            String image, double srp, Date avail, String isbn,
            int page, String backing, String dimensions, Author author
            ) {
        this.id = id;
        this.title = title;
        this.pubDate = pubDate;
        this.publisher = publisher;
        this.subject = subject;
        this.desc = desc;
        this.related1 = null;
        this.related2 = null;
        this.related3 = null;
        this.related4 = null;
        this.related5 = null;
        this.thumbnail = thumbnail;
        this.image = image;
        this.srp = srp;
        this.avail = avail;
        this.isbn = isbn;
        this.page = page;
        this.backing = backing;
        this.dimensions = dimensions;
        this.author = author;
    }

    /**
     * Getter do atributo <code>title</code>.
     * 
     * @return o <code>title</code> de um <code>Book</code>.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Getter do atributo <code>thumbnail</code>.
     * 
     * @return o <code>thumbnail</code> de um <code>Book</code>.
     */
    public String getThumbnail() {
        return thumbnail;
    }

    /**
     * Setter do atributo <code>thumbnail</code>.
     * 
     * @param thumbnail o caminho para um <code>thumbnail</code>.
     */
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }


    /**
     * Getter do atributo <code>image</code>.
     * 
     * @return o <code>image</code> de um <code>Book</code>.
     */
    public String getImage() {
        return image;
    }

    /**
     * Setter do atributo <code>image</code>.
     * 
     * @param image o caminho para um <code>image</code>.
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * Getter do atributo <code>author</code>.
     * 
     * @return o <code>author</code> de um <code>Book</code>.
     */
    public Author getAuthor() {
        return author;
    }

    /**
     * Getter do atributo <code>srp</code>.
     * 
     * @return o <code>srp</code> de um <code>Book</code>.
     */
    public double getSrp() {
        return srp;
    }


    /**
     * Getter do atributo <code>desc</code>.
     * 
     * @return o <code>desc</code> de um <code>Book</code>.
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Getter do atributo <code>page</code>.
     * 
     * @return o <code>page</code> de um <code>Book</code>.
     */
    public int getPage() {
        return page;
    }

    /**
     * Getter do atributo <code>backing</code>.
     * 
     * @return o <code>backing</code> de um <code>Book</code>.
     */
    public String getBacking() {
        return backing;
    }

    /**
     * Getter do atributo <code>pubDate</code>.
     * 
     * @return o <code>pubDate</code> de um <code>Book</code>.
     */
    public Date getPubDate() {
        return pubDate;
    }

    /**
     * Setter do atributo <code>pubDate</code>.
     * 
     * @param pubDate o caminho para um <code>pubDate</code>.
     */
    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    /**
     * Getter do atributo <code>publisher</code>.
     * 
     * @return o <code>publisher</code> de um <code>Book</code>.
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * Getter do atributo <code>isbn</code>.
     * 
     * @return o <code>isbn</code> de um <code>Book</code>.
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * Getter do atributo <code>id</code>.
     * 
     * @return o <code>id</code> de um <code>Book</code>.
     */
    public int getId() {
        return id;
    }

    /**
     * Getter do atributo <code>dimensions</code>.
     * 
     * @return o <code>dimensions</code> de um <code>Book</code>.
     */
    public String getDimensions() {
        return dimensions;
    }

    /**
     * Getter do atributo <code>subject</code>.
     * 
     * @return o <code>subject</code> de um <code>Book</code>.
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Getter do atributo <code>avail</code>.
     * 
     * @return o <code>avail</code> de um <code>Book</code>.
     */
    public Date getAvail() {
        return avail;
    }

    /**
     * Getter do atributo <code>related1</code>.
     * 
     * @return o <code>related1</code> de um <code>Book</code>.
     */
    public Book getRelated1() {
        return related1;
    }

    /**
     * Getter do atributo <code>related2</code>.
     * 
     * @return o <code>related2</code> de um <code>Book</code>.
     */
    public Book getRelated2() {
        return related2;
    }

    /**
     * Getter do atributo <code>related3</code>.
     * 
     * @return o <code>related3</code> de um <code>Book</code>.
     */
    public Book getRelated3() {
        return related3;
    }

    /**
     * Getter do atributo <code>related4</code>.
     * 
     * @return o <code>related4</code> de um <code>Book</code>.
     */
    public Book getRelated4() {
        return related4;
    }

    /**
     * Getter do atributo <code>related5</code>.
     * 
     * @return o <code>related5</code> de um <code>Book</code>.
     */
    public Book getRelated5() {
        return related5;
    }

    /**
     * Setter do atributo <code>related1</code>.
     * 
     * @param related1 o caminho para um <code>related1</code>.
     */
    public void setRelated1(Book related1) {
        this.related1 = related1;
    }

    /**
     * Setter do atributo <code>related2</code>.
     * 
     * @param related2 o caminho para um <code>related2</code>.
     */
    public void setRelated2(Book related2) {
        this.related2 = related2;
    }

    /**
     * Setter do atributo <code>related3</code>.
     * 
     * @param related3 o caminho para um <code>related3</code>.
     */
    public void setRelated3(Book related3) {
        this.related3 = related3;
    }

    /**
     * Setter do atributo <code>related4</code>.
     * 
     * @param related4 o caminho para um <code>related4</code>.
     */
    public void setRelated4(Book related4) {
        this.related4 = related4;
    }

    /**
     * Setter do atributo <code>related5</code>.
     * 
     * @param related5 o caminho para um <code>related5</code>.
     */
    public void setRelated5(Book related5) {
        this.related5 = related5;
    }

    /**
     * Método para comparar objetos <code>Book</code>.
     * 
     * @param o um objeto qualquer para ser comparado.
     * @return true se os objetos são iguais. false se são diferentes.
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof Book) {
            Book book = (Book) o;
            return id == book.id;
        }
        return false;
    }

    /**
     * Método para gerar um hashCode para um <code>Book</code>.
     * 
     * @return um valor inteiro.
     */
    @Override
    public int hashCode() {
        return id;
    }

    /**
     * Método sobrescrito de <code>toString()</code>
     */
	@Override
	public String toString() {
		return "Book [id=" + id + ", title=" + title + ", pubDate=" + pubDate + ", subject=" + subject + ", author="
				+ author + "]";
	}

}
