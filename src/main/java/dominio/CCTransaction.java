package dominio;

/* 
 * CCTransaction.java - Class holds data for a single credit card
 *                      transaction.
 *
 ************************************************************************
 *
 * This is part of the the Java TPC-W distribution,
 * written by Harold Cain, Tim Heil, Milo Martin, Eric Weglarz, and Todd
 * Bezenek.  University of Wisconsin - Madison, Computer Sciences
 * Dept. and Dept. of Electrical and Computer Engineering, as a part of
 * Prof. Mikko Lipasti's Fall 1999 ECE 902 course.
 *
 * Copyright (C) 1999, 2000 by Harold Cain, Timothy Heil, Milo Martin, 
 *                             Eric Weglarz, Todd Bezenek.
 * Copyright © 2008 Gustavo Maciel Dias Vieira
 *
 * This source code is distributed "as is" in the hope that it will be
 * useful.  It comes with no warranty, and no author or distributor
 * accepts any responsibility for the consequences of its use.
 *
 * Everyone is granted permission to copy, modify and redistribute
 * this code under the following conditions:
 *
 * This code is distributed for non-commercial use only.
 * Please contact the maintainer for restrictions applying to 
 * commercial use of these tools.
 *
 * Permission is granted to anyone to make or distribute copies
 * of this code, either as received or modified, in any
 * medium, provided that all copyright notices, permission and
 * nonwarranty notices are preserved, and that the distributor
 * grants the recipient permission for further redistribution as
 * permitted by this document.
 *
 * Permission is granted to distribute this code in compiled
 * or executable form under the same conditions that apply for
 * source code, provided that either:
 *
 * A. it is accompanied by the corresponding machine-readable
 *    source code,
 * B. it is accompanied by a written offer, with no time limit,
 *    to give anyone a machine-readable copy of the corresponding
 *    source code in return for reimbursement of the cost of
 *    distribution.  This written offer must permit verbatim
 *    duplication by anyone, or
 * C. it is distributed by someone who received only the
 *    executable form, and is accompanied by a copy of the
 *    written offer of source code that they received concurrently.
 *
 * In other words, you are welcome to use, share and improve this codes.
 * You are forbidden to forbid anyone else to use, share and improve what
 * you give them.
 *
 ************************************************************************/
import java.io.Serializable;
import java.util.Date;

/**
 * CCTransation.java - classe responsável por representar uma transaçao por cartão de crédito.
 * 
 * <p>Esta classe é composta pelos atributos, métodos getters e um construtor.</p>
 * <br>
 * *<img src="./doc-files/CCTransaction.png" alt="CCTransaction">
 * <br><a href="./doc-files/CCTransaction.html"> code </a>
 */
public class CCTransaction implements Serializable {

    private static final long serialVersionUID = 5470177450411822726L;

    private final String type; // o tipo de cartão de crédito de <code>CCTransaction</code>.
    private final long num; // o número de cartão de crédito de <code>CCTransaction</code>.
    private final String name; // o nome do titulo do cartão de crédito de <code>CCTransaction</code>.
    private final Date expire; // a data de expiração do cartão de crédito de <code>CCTransaction</code>.
    private final String authId; // autorização para a transação de <code>CCTransaction</code>.
    private final double amount; // o valor da transação de <code>CCTransaction</code>.
    private final Date date; // a data da transação de <code>CCTransaction</code>.
    private final Country country; // o país onde a transação foi feita para <code>CCTransaction</code>.

    /**
     * Construtor da classe.
     * 
     * @param type o tipo de cartão de crédito de <code>CCTransaction</code>.
     * @param num o número de cartão de crédito de <code>CCTransaction</code>.
     * @param name o nome do titulo do cartão de crédito de <code>CCTransaction</code>.
     * @param expire a data de expiração do cartão de crédito de <code>CCTransaction</code>.
     * @param authId autorização para a transação de <code>CCTransaction</code>.
     * @param amount o valor da transação de <code>CCTransaction</code>.
     * @param date a data da transação de <code>CCTransaction</code>.
     * @param country o país onde a transação foi feita para <code>CCTransaction</code>.
     */
    public CCTransaction(String type, long num, String name, Date expire,
            String authId, double amount, Date date, Country country) {
        this.type = type;
        this.num = num;
        this.name = name;
        this.expire = expire;
        this.authId = authId;
        this.amount = amount;
        this.date = date;
        this.country = country;
    }

    /**
     * Getter do atributo <code>type</code>.
     * 
     * @return o <code>type</code> de um <code>CCTransaction</code>.
     */
    public String getType() {
        return type;
    }

    /**
     * Getter do atributo <code>num</code>.
     * 
     * @return o <code>num</code> de um <code>CCTransaction</code>.
     */
    public long getNum() {
        return num;
    }

    /**
     * Getter do atributo <code>name</code>.
     * 
     * @return o <code>name</code> de um <code>CCTransaction</code>.
     */
    public String getName() {
        return name;
    }

    /**
     * Getter do atributo <code>expire</code>.
     * 
     * @return o <code>expire</code> de um <code>CCTransaction</code>.
     */
    public Date getExpire() {
        return expire;
    }

    /**
     * Getter do atributo <code>authId</code>.
     * 
     * @return o <code>authId</code> de um <code>CCTransaction</code>.
     */
    public String getAuthId() {
        return authId;
    }

    /**
     * Getter do atributo <code>amount</code>.
     * 
     * @return o <code>amount</code> de um <code>CCTransaction</code>.
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Getter do atributo <code>date</code>.
     * 
     * @return o <code>date</code> de um <code>CCTransaction</code>.
     */
    public Date getDate() {
        return date;
    }

    /**
     * Getter do atributo <code>country</code>.
     * 
     * @return o <code>country</code> de um <code>CCTransaction</code>.
     */
    public Country getCountry() {
        return country;
    }

}
