package recommendation;

import java.util.List;

import dominio.Book;

/**
 * Recommendation interface definition.
 * <br>
 * *<img src="./doc-files/Recommendation.png" alt="Recommendation">
 */
public interface Recommendation {
	List<Book> itemBased(int itemId);
	List<Book> userBased(int userId);
}
