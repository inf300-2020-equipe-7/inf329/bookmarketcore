package recommendation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.GenericItemSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.LogLikelihoodSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dominio.Book;
import servico.Bookstore;

/**
 *  RecommendationImpl.java - classe responsável pela implementação do módulo de recomendação.
 *	<br>
 *  *<img src="./doc-files/RecommendationImpl.png" alt="RecommendationImpl">
 *  <br><a href="./doc-files/RecommendationImpl.html"> code </a>
 */
public class RecommendationImpl implements Recommendation {
	private static final Logger logger = LoggerFactory.getLogger(RecommendationImpl.class);

	/**
	 * Método privado que retorna um <code>RecommenderBuilder</code> para usuários a partir de um <code>DataModel</code>.
	 * 
	 * <p>Este método cria similaridades utilizando o algoritmo de <code>PearsonCorrelationSimilarity</code>.</p>
	 * <p>Além disso, para se criar uma <em>neighborhood</em> utilizamos o <code>NearestNUserNeighborhood</code> com tamanho de 100.</p>
	 * 
	 * @param model o <code>DataModel</code> que será trabalhado.
	 * @return um <code>RecommenderBuilder</code> que pode ser utilizado para gerar recomendações.
	 */
	private RecommenderBuilder userRecommender(DataModel model) {
		logger.info("userRecommender");
		
		return (dataModel) -> {
			logger.info("Users: {} Items: {}", model.getNumUsers(), model.getNumItems());
			
			UserSimilarity similarity = new PearsonCorrelationSimilarity(model);
			UserNeighborhood neighborhood = new NearestNUserNeighborhood(100, similarity, model);// neighborhood-size,similarity,
																								// model
			return new GenericUserBasedRecommender(model, neighborhood, similarity);
		};
	}

	/**
	 * Método privado que retorna um <code>RecommenderBuilder</code> para itens a partir de um <code>DataModel</code>.
	 * 
	 * <p>Este método cria similaridades utilizando o algoritmo de <code>LogLikelihoodSimilarity</code>.</p>
	 * 
	 * @param model o <code>DataModel</code> que será trabalhado.
	 * @return um <code>RecommenderBuilder</code> que pode ser utilizado para gerar recomendações.
	 * @throws TasteException caso ocorra algum erro
	 */
	private RecommenderBuilder itemRecommender(DataModel model) throws TasteException {
		logger.info("itemRecommender");
		
		return (dataModel) -> {
			logger.info("Users: {} Items: {}", model.getNumUsers(), model.getNumItems());
			
			ItemSimilarity logLikeSimilarity = new LogLikelihoodSimilarity(model);
			ItemSimilarity similarity = new GenericItemSimilarity(logLikeSimilarity, model);
			return new GenericItemBasedRecommender(model, similarity);
		};
	}

	/**
	 * Método privado para gerar um <code>DataModel</code> para a partir de um arquivo de dataset.
	 *  
	 * @param filePath o caminho para um arquivo de dataset.
	 * @return o <code>DataModel</code> gerado.
	 */
	private DataModel getDataModel(String filePath) {
		DataModel model = null;
        try {
			model = new FileDataModel(new File(filePath));
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return model;
	}

	/**
	 * Método que retorna as recomendações de livros com semelhanças entre itens.
	 * 
	 * <p>Este método segue o seguinte algoritmo:</p>
	 * <ol>
	 *   <li>Monta-se um <code>DataModel</code> com os dados existentes no arquivo de dataset mais recente;</li>
	 *   <li>Monta-se um <code>RecommenderBuilder</code> utilizando o algoritmo <code>LogLikelihoodSimilarity</code>
	 *    para geração de similaridades;</li>
	 *   <li>Geramos uma recomendação de 5 (cinco) itens;</li>
	 *   <li>Retornamos um array com os itens de recomendação.</li>
	 * </ol>
	 * <br>
	 * @param itemID o id de um <code>Book</code>
	 * @return uma lista de recomendações com objetos <code>Book</code>.
	 */
	@Override
	public List<Book> itemBased(int itemID) {
		DataModel model = getDataModel(Bookstore.getDatasetFiles().get(0));
		List<Book> books = new ArrayList<>();

		try {
			books = this.itemRecommender(model)
					.buildRecommender(model)
					.recommend(itemID, 5)
					.stream()
					.map(r -> {
						int id = Long.valueOf(r.getItemID()).intValue();
						logger.info(Bookstore.getBook(id).get().toString());
						return Bookstore.getBook(id).orElse(null);
					}).collect(Collectors.toList());
		} catch (TasteException e) {
			logger.error(e.getMessage());
		}
		return books;
	}

	/**
	 * Método que retorna as recomendações de livros com semelhanças entre usuários.
	 * 
	 * <p>Este método segue o seguinte algoritmo:</p>
	 * <ol>
	 *   <li>Monta-se um <code>DataModel</code> com os dados existentes no arquivo de dataset mais recente;</li>
	 *   <li>Monta-se um <code>RecommenderBuilder</code> utilizando os algoritmos <code>PearsonCorrelationSimilarity</code>
	 *    para geração de similaridades e <code>NearestNUserNeighborhood</code> para determinação de vizinhanças;</li>
	 *   <li>Geramos uma recomendação de 5 (cinco) itens;</li>
	 *   <li>Retornamos um array com os itens de recomendação.</li>
	 * </ol>
	 * <br>
	 * @param userID o id de um <code>Customer</code>
	 * @return uma lista de recomendações com objetos <code>Book</code>.
	 */
	@Override
	public List<Book> userBased(int userID) {
		DataModel model = getDataModel(Bookstore.getDatasetFiles().get(0));
		List<Book> books = new ArrayList<>();
		
		try {
			books = this.userRecommender(model)
							.buildRecommender(model)
							.recommend(userID, 5)
							.stream()
							.map(r -> {
								int id = Long.valueOf(r.getItemID()).intValue();
								logger.info(Bookstore.getBook(id).get().toString());
								return Bookstore.getBook(id).orElse(null);
							}).collect(Collectors.toList());
		} catch (TasteException e) {
			logger.error(e.getMessage());
		}
		return books;
	}
}
