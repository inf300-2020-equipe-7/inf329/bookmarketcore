package mahout;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.List;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.IRStatistics;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderEvaluator;
import org.apache.mahout.cf.taste.eval.RecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.common.FastByIDMap;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.impl.common.RunningAverage;
import org.apache.mahout.cf.taste.impl.eval.AverageAbsoluteDifferenceRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.eval.GenericRecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.eval.LoadEvaluator;
import org.apache.mahout.cf.taste.impl.eval.LoadStatistics;
import org.apache.mahout.cf.taste.impl.model.GenericDataModel;
import org.apache.mahout.cf.taste.impl.model.GenericPreference;
import org.apache.mahout.cf.taste.impl.model.GenericUserPreferenceArray;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.EuclideanDistanceSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.model.PreferenceArray;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.apache.mahout.common.RandomUtils;
import org.junit.Test;

/**
 * Recommended class:
 * PearsonCorrelationSimilarity;
 * GenericUserBasedRecommender;
 * NearestNUserNeighborhood;
 * 
 * @see <a href="https://moodle.lab.ic.unicamp.br/moodle/pluginfile.php/16048/mod_resource/content/5/inf329project.pdf">Projeto Bookmarket</a>
 *
 *Concepts:
 *
 *UserSimilarity -> defines a notion of similarity between tow Users.
 *There are following difinition of similarity available, which are:
 *- Person Correlation
 *- Spearman Correlation
 *- Euclidean Distance
 *- Tanimoto Coefficient
 *- LogLikelihood Similarity
 *
 *Recommendation Engine
 *- GenericUserbasedRecommender ->User Similarity metric; Conventional implementation; Fast when number of users is relatively small
 *- GenericItemBasedRecommender ->Item similarity metric; Fast when number of items i relatively small.
 *- SlopeOneRecommender -> Diff storage strategy. Recommendations and updates are fast at runtime. Requires large precomputation. Suitable when number of items is relatively small.
 *						   (it is very fast and simple item-based recommendation approach applicable when users have given ratings)
 *- SVDRecommender -> Required large precomputation
 *- KnnItemBasedRecommender-> 
 *- TreeClusteringRecommender ->
 *
 *
 *In this project we will make use of two recommender algorithm class Implementation which are: GenericUserBasedRecommender and GenericItemBasedRecommender
 *
 *for User recommendation we will use "person correlation similarity based on nearest N user neighborhood similarity" and for item ItemSimilarity
 *based on @author - Guilherme <Euclidean distance similarity>. 
 */
public class Recommendation {
	
	/**
	 * User recommendation test using Person correlation similarity based n-User neighborhood
	 * @throws TasteException
	 * @throws IOException
	 */	
	@Test
	public void testUserRecommendationFromFile() throws TasteException, IOException {
		RandomUtils.useTestSeed();//Ensures the consistency between different evaluation runs
		
		System.out.println("***Person correlation similarity based n-User neighborhood***");
		int userID = 2;
		int nRecommendation = 3;
		DataModel model = dummyDataSetFromFile();
		
		RecommenderBuilder recommenderBuilder = userRecommenderBuilderPearsonCorrelationSimilarityBasedNearestNUserNeighborhood(model);		
		Recommender recommender = recommenderBuilder.buildRecommender(model);
		
		List<RecommendedItem> recommendations = userSimilarity(recommender, userID, nRecommendation);
		recommendations.forEach((r) -> System.out.println(r));
		assertEquals(2, recommendations.size());
		
		assertEquals(3, recommendations.get(0).getItemID(), 1);
		assertEquals(4.5, recommendations.get(0).getValue(), 1);
		
		assertEquals(4, recommendations.get(1).getItemID(), 1);
		assertEquals(4.0, recommendations.get(1).getValue(), 1);
		
		
		RecommenderEvaluator evaluator = new AverageAbsoluteDifferenceRecommenderEvaluator();
		double score = evaluator.evaluate(recommenderBuilder, null, model, 0.95, 0.05); //null=dataModelBuilder; 95%=trainingPercentage; 5%=evaluationPercentage
		System.out.println("RecommenderEvaluator score: " + score);
		
		RecommenderIRStatsEvaluator irEvaluator = new GenericRecommenderIRStatsEvaluator();
		IRStatistics stats = irEvaluator.evaluate(recommenderBuilder, null, model, null, 5, GenericRecommenderIRStatsEvaluator.CHOOSE_THRESHOLD, 1);
		System.out.println("IR stats Precision: " + stats.getPrecision());
		System.out.println("IR stats Recall: " + stats.getRecall());
		System.out.println("IR stats F1-Measure: " + stats.getF1Measure());
		
		printStatistics(recommender);
		System.out.println(" ================================================================================");
		
	}
	
	/**
	 * User recommendation test using Person correlation similarity based n-User neighborhood
	 * @throws TasteException
	 * @throws IOException
	 */	
	@Test
	public void testUserRecommendationFromFile2() throws TasteException, IOException {
		RandomUtils.useTestSeed();//Ensures the consistency between different evaluation runs
		
		System.out.println("***Person correlation similarity based n-User ThresholdUserNeighborhood***");
		int userID = 2;
		int nRecommendation = 3;
		DataModel model = dummyDataSetFromFile();
		
		RecommenderBuilder recommenderBuilder = userRecommenderBuilderPearsonCorrelationSimilarityBasedThresholdUserNeighborhood(model);		
		Recommender recommender = recommenderBuilder.buildRecommender(model);
		
		List<RecommendedItem> recommendations = userSimilarity(recommender, userID, nRecommendation);
		recommendations.forEach((r) -> System.out.println(r));
		assertEquals(0, recommendations.size());
		
		
		RecommenderEvaluator evaluator = new AverageAbsoluteDifferenceRecommenderEvaluator();
		double score = evaluator.evaluate(recommenderBuilder, null, model, 0.95, 0.05); //null=dataModelBuilder; 95%=trainingPercentage; 5%=evaluationPercentage
		System.out.println("RecommenderEvaluator score = " + score);
		
		RecommenderIRStatsEvaluator irEvaluator = new GenericRecommenderIRStatsEvaluator();
		IRStatistics stats = irEvaluator.evaluate(recommenderBuilder, null, model, null, 5, GenericRecommenderIRStatsEvaluator.CHOOSE_THRESHOLD, 1);
		System.out.println("IR stats Precision" + stats.getPrecision());
		System.out.println("IR stats Recall" + stats.getRecall());
		System.out.println("IR stats F1-Measure" + stats.getF1Measure());
		
		printStatistics(recommender);
		System.out.println(" ================================================================================");
		
	}

	@Test
	public void testRecommendationFromDummy() throws TasteException, IOException {
		System.out.println("testRecommendationFromDummy");
		int userID = 2;
		int nRecommendation = 3;
		
		DataModel model = dummyDataSetFromFile();
		
		RecommenderBuilder recommenderBuilder = userRecommenderBuilderPearsonCorrelationSimilarityBasedNearestNUserNeighborhood(model);		
		Recommender recommender = recommenderBuilder.buildRecommender(model);
		
		List<RecommendedItem> recommendations = userSimilarity(recommender, userID, nRecommendation);

		
		recommendations.forEach((r) -> System.out.println(r));
		assertEquals(2, recommendations.size());

		assertEquals(3, recommendations.get(0).getItemID(), 1);
		assertEquals(4.5, recommendations.get(0).getValue(), 1);

		assertEquals(4, recommendations.get(1).getItemID(), 1);
		assertEquals(4.0, recommendations.get(1).getValue(), 1);
		System.out.println(" ================================================================================");
	}
	
	private DataModel dummyDataSetFromFile() throws IOException {	
		String file = getClass().getResource("../dataset").getFile();
		return new FileDataModel(new File(file));

	}
	
	private DataModel createDummyDataModel() throws TasteException {
		int size = 5;
		FastByIDMap<PreferenceArray> map = new FastByIDMap<>();
		PreferenceArray user1Pref = new GenericUserPreferenceArray(size);
		PreferenceArray user2Pref = new GenericUserPreferenceArray(size);
		PreferenceArray user3Pref = new GenericUserPreferenceArray(size);
		PreferenceArray user4Pref = new GenericUserPreferenceArray(size);

		// User1, each position has a Preference(userId, ItemId, Preference value/score)
		user1Pref.set(0, new GenericPreference(1L, 0L, 1.0f));
		user1Pref.set(1, new GenericPreference(1L, 1L, 2.0f));
		user1Pref.set(2, new GenericPreference(1L, 2L, 5.0f));
		user1Pref.set(3, new GenericPreference(1L, 3L, 5.0f));
		user1Pref.set(4, new GenericPreference(1L, 4L, 5.0f));

		// User2
		user2Pref.set(0, new GenericPreference(2L, 0L, 1.0f));
		user2Pref.set(1, new GenericPreference(2L, 1L, 2.0f));
		user2Pref.set(2, new GenericPreference(2L, 5L, 5.0f));
		user2Pref.set(3, new GenericPreference(2L, 6L, 4.5f));
		user2Pref.set(4, new GenericPreference(2L, 2L, 5.0f));

		// User3
		user3Pref.set(0, new GenericPreference(3L, 1L, 2.5f));
		user3Pref.set(1, new GenericPreference(3L, 2L, 5.0f));
		user3Pref.set(2, new GenericPreference(3L, 3L, 4.0f));
		user3Pref.set(3, new GenericPreference(3L, 4L, 3.0f));

		// User4
		user4Pref.set(0, new GenericPreference(4L, 0L, 5.0f));
		user4Pref.set(1, new GenericPreference(4L, 1L, 5.0f));
		user4Pref.set(2, new GenericPreference(4L, 2L, 5.0f));
		user4Pref.set(3, new GenericPreference(4L, 3L, 0.0f));

		// map.put(userId, PreferenceArray)
		map.put(1, user1Pref);
		map.put(2, user2Pref);
		map.put(3, user3Pref);
		map.put(4, user4Pref);

		DataModel model = new GenericDataModel(map);

		for (LongPrimitiveIterator it = model.getUserIDs(); it.hasNext(); ) {
			Long id = it.next();
			System.out.println("UserId: " + id);
		}

		return model;
	}
	
	/**
	 * There are two types of neighborhoods:
	 * 
	 * NearestNUserNeighborhood - This class computes a neighborhood consisting of the nearest n 
	 * users to a given user. "Nearest" is defined by the given UserSimilarity.
	 * 
	 * ThresholdUserNeighborhood - This class computes a neighborhood consisting of all the users 
	 * whose similarity to the given user meets or exceeds a certain threshold. Similarity is defined by the given UserSimilarity.
	 * 
	 * @param model
	 * @throws TasteException
	 */
	public List<RecommendedItem> userSimilarity(Recommender recommender, long userID, int nRecommendation) throws TasteException {
		Instant startTime = Instant.now();
		System.out.println("Recommendation start time: " + startTime);
		List<RecommendedItem> recommendations = recommender.recommend(userID, nRecommendation);//userID, desiredNumberOfRecommendation
		
		Instant endTime = Instant.now();
		System.out.println("Recommendation end: " + endTime);
		System.out.println("Duration (ms) : "+ Duration.between(startTime, endTime).toMillis());
		return recommendations;

	}
	
	/**
	 * PearsonCorrelationSimilarity builder based on NearestNUserNeighborhood
	 * @param model
	 * @return
	 */
	private RecommenderBuilder userRecommenderBuilderPearsonCorrelationSimilarityBasedNearestNUserNeighborhood(DataModel model) {
		return (dataModel) -> {
				System.out.println("User: " + model.getNumUsers() + " Items: " + model.getNumItems());
				UserSimilarity similarity = new PearsonCorrelationSimilarity(model);
				UserNeighborhood neighborhood = new NearestNUserNeighborhood(3, similarity, model);//neighborhood-size,similarity, model
				
				return new GenericUserBasedRecommender(model, neighborhood, similarity);			
		};
	}
	
	/**
	 * PearsonCorrelationSimilarity builder based on ThresholdUserNeighborhood
	 * @param model
	 * @return
	 */
	private RecommenderBuilder userRecommenderBuilderPearsonCorrelationSimilarityBasedThresholdUserNeighborhood(DataModel model) {
		return (dataModel) -> {
				System.out.println("User: " + model.getNumUsers() + " Items: " + model.getNumItems());
				UserSimilarity similarity = new PearsonCorrelationSimilarity(model);
				UserNeighborhood neighborhood = new ThresholdUserNeighborhood(3.0, similarity, model);
				
				return new GenericUserBasedRecommender(model, neighborhood, similarity);			
		};
	}
	
	/**
	 * EuclideanDistanceSimilarity based on NearestNUserNeighborhood
	 * @param model
	 * @return
	 */
	private RecommenderBuilder userRecommenderBuilderEuclideanDistanceSimilarityBasedNearestNUserNeighborhood(DataModel model) {
		return (dataModel) -> {
				System.out.println("User: " + model.getNumUsers() + " Items: " + model.getNumItems());
				UserSimilarity similarity = new EuclideanDistanceSimilarity(model);
				UserNeighborhood neighborhood = new NearestNUserNeighborhood(3, similarity, model);//neighborhood-size,similarity, model
				
				return new GenericUserBasedRecommender(model, neighborhood, similarity);			
		};
	}
	
	/**
	 * EuclideanDistanceSimilarity based on ThresholdUserNeighborhood
	 * @param model
	 * @return
	 */
	private RecommenderBuilder userRecommenderBuilderEuclideanDistanceSimilarityBasedThresholdUserNeighborhood(DataModel model) {
		return (dataModel) -> {
				System.out.println("User: " + model.getNumUsers() + " Items: " + model.getNumItems());
				UserSimilarity similarity = new EuclideanDistanceSimilarity(model);
				UserNeighborhood neighborhood = new ThresholdUserNeighborhood(3.0, similarity, model);		
				
				return new GenericUserBasedRecommender(model, neighborhood, similarity);			
		};
	}
	
	
	public void printStatistics(Recommender recommender) throws TasteException {
		LoadStatistics statistics = LoadEvaluator.runLoad(recommender);
		RunningAverage timing = statistics.getTiming();
		System.out.println("Timing avarege: " + timing.getAverage());
		System.out.println("TIming getCount: "+ timing.getCount());

	}
	
	

}
