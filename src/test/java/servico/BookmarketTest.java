/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servico;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import dominio.Book;
import dominio.BookRating;
import dominio.Cart;
import dominio.Customer;
import dominio.Order;
import dominio.Stock;


/**
 *
 * @author INF329
 */
public class BookmarketTest {
	
    static Bookmarket instance;
    static int customers = 1000;
    
    public BookmarketTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        Bookstore store = new Bookstore(0);
        Bookstore store1 = new Bookstore(1);
    	Bookstore[] state = {store, store1};
    	instance.init(state);
		int items = 1000;
		int addresses = 1000;
		int authors = 100;
		int orders = 10000;
		instance.populate(items, customers, addresses, authors, orders);
    }
    
    @AfterClass
    public static void tearDownClass() {
    	instance = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of init method, of class Bookmarket. Throws a RuntimeException.
     */
     @Test(expected = RuntimeException.class)
    public void testInitThrowsRuntimeException() {
        System.out.println("initThrowsRuntimeException");
        Bookstore[] state = null;
        Bookmarket.init(state);
    }

    /**
     * Test of getCustomer method, of class Bookmarket.
     */
    @Test
    public void testGetCustomer() {
        System.out.println("getCustomer");
        
        String UNAME = Bookmarket.getUserName(0);
        Customer expResult = Bookmarket.getCustomer(UNAME);
        Customer result = Bookmarket.getCustomer(UNAME);
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class Bookmarket.
     */
     @Test
    public void testGetName() {
        System.out.println("getName");
        int c_id = 0;
        String username = Bookmarket.getUserName(c_id);
        Customer customer = Bookmarket.getCustomer(username);

        String[] expResult = {customer.getFname(), customer.getLname(), username};
        String[] result = Bookmarket.getName(c_id);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getUserName method, of class Bookmarket.
     */
     @Test
    public void testGetUserName() {
        System.out.println("getUserName");
        int c_id = 0;
        String username = Bookmarket.getUserName(c_id);
        Customer customer = Bookmarket.getCustomer(username);

        String expResult = customer.getUname();
        String result = Bookmarket.getUserName(c_id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPassword method, of class Bookmarket.
     */
     @Test
    public void testGetPassword() {
        System.out.println("getPassword");
        int c_id = 0;
        String username = Bookmarket.getUserName(c_id);
        Customer customer = Bookmarket.getCustomer(username);

        String expResult = customer.getPasswd();
        String result = Bookmarket.getPassword(username);
        assertEquals(expResult, result);
    }

    /**
     * Test of getMostRecentOrder method, of class Bookmarket.
     */
     @Test
    public void testGetMostRecentOrder() {
        System.out.println("getMostRecentOrder");
        int c_id = 0;
        String username = Bookmarket.getUserName(c_id);
        Customer customer = Bookmarket.getCustomer(username);

        Order expResult = customer.getMostRecentOrder();
        Order result = Bookmarket.getMostRecentOrder(username);
        assertEquals(expResult, result);
    }

    /**
     * Test of createNewCustomer method, of class Bookmarket.
     */
     @Test
    public void testCreateNewCustomer() {
        System.out.println("createNewCustomer");
        String fname = "TestFname";
        String lname = "TestLname";
        String street1 = "TestStreet1";
        String street2 = "TestStreet2";
        String city = "TestCity";
        String state = "TestState";
        String zip = "TestZip";
        String countryName = "TestCountryName";
        String phone = "TestPhone";
        String email = "TestEmail";
        Date birthdate = new Date(System.currentTimeMillis());
        String data = "TestData";

        Customer result = Bookmarket.createNewCustomer(fname, lname, street1, street2, city, state, zip, countryName, phone, email, birthdate, data);
        Customer expResult = Bookmarket.getCustomer(Bookmarket.getUserName(result.getId()));
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createNewCustomer method, of class Bookmarket. Throws a RuntimeException.
     */
    @Test(expected = RuntimeException.class)
    public void testCreateNewCustomerThrowsRuntimeException() {
    	System.out.println("createNewCustomerThrowsRuntimeException");
    	String fname = null;
        String lname = null;
        String street1 = null;
        String street2 = null;
        String city = null;
        String state = null;
        String zip = null;
        String countryName = null;
        String phone = null;
        String email = null;
        Date birthdate = null;
        String data = null;
        
        Bookmarket.createNewCustomer(fname, lname, street1, street2, city, state, zip, countryName, phone, email, birthdate, data);
    }

    /**
     * Test of refreshSession method, of class Bookmarket.
     */
     @Test
    public void testRefreshSession() {
        System.out.println("refreshSession");
        int c_id = 0;
        Customer customer = Bookmarket.getCustomer(Bookmarket.getUserName(c_id));

        Date expResult1 = customer.getLogin();
        Bookmarket.refreshSession(c_id);
        
        assertFalse(expResult1.equals(customer.getLogin()));
        assertFalse(expResult1.equals(customer.getExpiration()));
    }
     
     /**
      * Test of refreshSession method, of class Bookmarket. Throws a RuntimeException.
      */
     @Test(expected = RuntimeException.class)
     public void testRefreshSessionThrowsRuntimeException() {
     	System.out.println("refreshSessionThrowsRuntimeException");
     	int c_id = -1;
     	Bookmarket.refreshSession(c_id);
    }

    /**
     * Test of getBook method, of class Bookmarket.
     */
    @Test
    public void testGetBook() {
        System.out.println("getBook");
        int c_id = 0;
        Book book = Bookmarket.getMostRecentOrder(Bookmarket.getUserName(c_id))
                                .getLines().get(0).getBook();

        int i_id = book.getId();
        Book expResult = book;
        Book result = Bookmarket.getBook(i_id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getABookAnyBook method, of class Bookmarket.
     */
    @Test
    public void testGetABookAnyBook() {
        System.out.println("getABookAnyBook");
        Book result = Bookmarket.getABookAnyBook();
        assertNotNull(result);
    }

    /**
     * Test of doSubjectSearch method, of class Bookmarket.
     */
    @Test
    public void testDoSubjectSearch() {
        System.out.println("doSubjectSearch");
        String search_key = "ARTS";
        List<Book> result = Bookmarket.doSubjectSearch(search_key);
        assertTrue(result.stream().allMatch(b -> b.getSubject() == search_key));
    }

    /**
     * Test of doTitleSearch method, of class Bookmarket.
     */
    @Test
    public void testDoTitleSearch() {
        System.out.println("doTitleSearch");
        String search_key = Bookmarket.getABookAnyBook().getSubject();
        Pattern regex = Pattern.compile("^" + search_key);

        List<Book> result = Bookmarket.doTitleSearch(search_key);
        assertTrue(result.stream().allMatch(b -> regex.matcher(b.getTitle()).matches()));
    }

    /**
     * Test of doAuthorSearch method, of class Bookmarket.
     */
    @Test
    public void testDoAuthorSearch() {
        System.out.println("doAuthorSearch");
        String search_key = Bookmarket.getABookAnyBook().getAuthor().getLname();
        Pattern regex = Pattern.compile("^" + search_key);

        List<Book> result = Bookmarket.doAuthorSearch(search_key);
        assertTrue(result.stream().allMatch(b -> regex.matcher(b.getAuthor().getLname()).matches()));
    }

    /**
     * Test of getNewProducts method, of class Bookmarket.
     */
    @Test
    public void testGetNewProducts() {
        System.out.println("getNewProducts");
        String subject = Bookmarket.getABookAnyBook().getSubject();

        List<Book> result = Bookmarket.getNewProducts(subject);
        int last = result.size() - 1;

        assertTrue(result.get(0).getPubDate().after(result.get(last).getPubDate()));
    }

    /**
     * Test of getCosts method, of class Bookmarket.
     */
     @Test
    public void testGetCosts() {
        System.out.println("getCosts");
        Book book = Bookmarket.getABookAnyBook();

        List<Double> result = Bookmarket.getCosts(book);
        assertTrue(result.stream().allMatch(cost -> cost > 0.0));
    }

    /**
     * Test of getBestSellers method, of class Bookmarket.
     */
     @Test
    public void testGetBestSellers() {
        System.out.println("getBestSellers");
        int quantidade1 = 1;
        List<Book> result1 = Bookmarket.getBestSellers(quantidade1);
        assertEquals(quantidade1, result1.size());
        
        int quantidade2 = 100;
        List<Book> result2 = Bookmarket.getBestSellers(quantidade2);
        assertEquals(quantidade2, result2.size());
        
        int quantidade3 = 0;
        List<Book> result3 = Bookmarket.getBestSellers(quantidade3);
        assertEquals(quantidade2, result3.size());
        
        int quantidade4 = 101;
        List<Book> result4 = Bookmarket.getBestSellers(quantidade4);
        assertEquals(quantidade2, result4.size());
    }

    /**
     * Test of getRecommendationByItens method, of class Bookmarket.
     */
     @Test
    public void testGetRecommendationByItens() {
        System.out.println("getRecommendationByItens");
        int c_id = 0;
        List<Book> expResult = null;
        List<Book> result = Bookmarket.getRecommendationByItens(c_id);
        
        assertThat(result, is(not(expResult)));
        assertEquals(true, result.size() <= 5);
    }

    /**
     * Test of getRecommendationByUsers method, of class Bookmarket.
     */
     @Test
    public void testGetRecommendationByUsers() {
        System.out.println("getRecommendationByUsers");
        int c_id = 0;
        List<Book> expResult = null;
        List<Book> result = Bookmarket.getRecommendationByUsers(c_id);
        
        assertThat(result, is(not(expResult)));
        assertEquals(true, result.size() <= 5);
    }

    /**
     * Test of getStocks method, of class Bookmarket.
     */
     @Test
    public void testGetStocks() {
        System.out.println("getStocks");
        int idBook = 0;
        
        List<Stock> expResult = new ArrayList<>();
        expResult.add(Bookmarket.getStock(0, idBook));
        expResult.add(Bookmarket.getStock(1, idBook));
        List<Stock> result = Bookmarket.getStocks(idBook);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getStock method, of class Bookmarket.
     */
     @Test
    public void testGetStock() {
        System.out.println("getStock");
        int idBookstore = 0;
        int idBook = 0;
        Stock expResult = Bookmarket.getStocks(idBook).get(0);
        Stock result = Bookmarket.getStock(idBookstore, idBook);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRelated method, of class Bookmarket.
     */
     @Test
    public void testGetRelated() {
        System.out.println("getRelated");
        int bId = 0;
        Book book = Bookmarket.getBook(bId);
        
        List<Book> expResult = new ArrayList<>(5);
        expResult.add(book.getRelated1());
        expResult.add(book.getRelated2());
        expResult.add(book.getRelated3());
        expResult.add(book.getRelated4());
        expResult.add(book.getRelated5());
        List<Book> result = Bookmarket.getRelated(bId);
        assertEquals(expResult, result);
    }

    /**
     * Test of adminUpdate method, of class Bookmarket.
     */
     @Test
    public void testAdminUpdate() {
        System.out.println("adminUpdate");
        int iId = 0;
        double cost = 0.0;
        String image = "";
        String thumbnail = "";
        Bookmarket.adminUpdate(iId, cost, image, thumbnail);
    }
     
    /**
     * Test of adminUpdate method, of class Bookmarket. Throws a RuntimeException.
     */
    @Test(expected = RuntimeException.class)
    public void testAdminUpdateThrowsRuntimeException() {
     	System.out.println("adminUpdateThrowsRuntimeException");
     	int iId = -1;
        double cost = 0.0;
        String image = null;
        String thumbnail = null;
        Bookmarket.adminUpdate(iId, cost, image, thumbnail);
    }

    /**
     * Test of createEmptyCart method, of class Bookmarket.
     */
     @Test
    public void testCreateEmptyCart() {
        System.out.println("createEmptyCart");
        int storeId = 0;
        int result = Bookmarket.createEmptyCart(storeId);
        int expResult = Bookmarket.getCart(storeId, result).getId();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createEmptyCart method, of class Bookmarket. Throws a RuntimeException.
     */
    @Test(expected = RuntimeException.class)
    public void testCreateEmptyCartThrowsRuntimeException() {
      	System.out.println("createEmptyCartThrowsRuntimeException");
      	int storeId = -1;
        Bookmarket.createEmptyCart(storeId);
    }

    /**
     * Test of doCart method, of class Bookmarket.
     */
     @Test
    public void testDoCart() {
        System.out.println("doCart");
        int storeId = 0;
        int cartId = Bookmarket.createEmptyCart(storeId);
        Book book = Bookmarket.getABookAnyBook();
        
        Cart expResult1 = Bookmarket.getCart(storeId, cartId);
        Cart result1 = Bookmarket.doCart(storeId, cartId, null, new ArrayList<>(), new ArrayList<>());
        assertEquals(1, result1.getLines().stream().findFirst().get().getQty());
        
        List<Integer> ids = new ArrayList<>();
        ids.add(Bookmarket.getABookAnyBook().getId());
        List<Integer> quantities = new ArrayList<>();
        quantities.add(1);
        Cart result2 = Bookmarket.doCart(storeId, cartId, book.getId(), ids, quantities);
        assertEquals(expResult1, result2);
    }
     
     /**
      * Test of doCart method, of class Bookmarket. Throws a RuntimeException.
      */
     @Test(expected = RuntimeException.class)
     public void testDoCartThrowsRuntimeException() {
       	System.out.println("doCartThrowsRuntimeException");
       	int storeId = 0;
        int SHOPPING_ID = 0;
        Integer I_ID = null;
        List<Integer> ids = null;
        List<Integer> quantities = null;
        Bookmarket.doCart(storeId, SHOPPING_ID, I_ID, ids, quantities);
     }

    /**
     * Test of getCart method, of class Bookmarket.
     */
     @Test
    public void testGetCart() {
        System.out.println("getCart");
        int storeId = 0;
        int SHOPPING_ID = Bookmarket.createEmptyCart(storeId);
        
        int expResult = SHOPPING_ID;
        Cart result = Bookmarket.getCart(storeId, SHOPPING_ID);
        assertEquals(expResult, result.getId());
    }

    /**
     * Test of doBuyConfirm_8args method, of class Bookmarket.
     */
     @Test
    public void testDoBuyConfirm_8args() {
        System.out.println("doBuyConfirm_8args");
        int storeId = 0;
        int shopping_id = Bookmarket.createEmptyCart(storeId);
        int customer_id = Bookmarket.getCustomer(Bookmarket.getUserName(0)).getId();
        List<Integer> ids = new ArrayList<>();
        ids.add(Bookmarket.getABookAnyBook().getId());
        List<Integer> quantities = new ArrayList<>();
        quantities.add(400);
        Bookmarket.doCart(storeId, shopping_id, Bookmarket.getABookAnyBook().getId(), 
        		ids, quantities);
        String cc_type = "";
        long cc_number = 0L;
        String cc_name = "";
        Date cc_expiry = null;
        String shipping = "";
        
        Order result = Bookmarket.doBuyConfirm(storeId, shopping_id, customer_id, cc_type, 
        		cc_number, cc_name, cc_expiry, shipping);
        Order expResult = Bookmarket.getMostRecentOrder(Bookmarket.getUserName(0));
        assertEquals(expResult, result);
    }
     
     /**
      * Test of doBuyConfirm_8args method, of class Bookmarket. Throws a RuntimeException.
      */
     @Test(expected = RuntimeException.class)
     public void testDoBuyConfirm_8argsThrowsRuntimeException() {
       	System.out.println("doBuyConfirm_8argsThrowsRuntimeException");
       	int storeId = -1;
        int shopping_id = -1;
        int customer_id = -1;
        String cc_type = null;
        long cc_number = 0L;
        String cc_name = null;
        Date cc_expiry = null;
        String shipping = null;
        Bookmarket.doBuyConfirm(storeId, shopping_id, customer_id, cc_type, 
        		cc_number, cc_name, cc_expiry, shipping);
     }

    /**
     * Test of doBuyConfirm_14args method, of class Bookmarket.
     */
     @Test
    public void testDoBuyConfirm_14args() {
        System.out.println("doBuyConfirm_14args");
        int storeId = 0;
        int shopping_id = Bookmarket.createEmptyCart(storeId);
        int customer_id = Bookmarket.getCustomer(Bookmarket.getUserName(0)).getId();
        List<Integer> ids = new ArrayList<>();
        ids.add(Bookmarket.getABookAnyBook().getId());
        List<Integer> quantities = new ArrayList<>();
        quantities.add(400);
        Bookmarket.doCart(storeId, shopping_id, Bookmarket.getABookAnyBook().getId(), 
        		ids, quantities);
        String cc_type = "";
        long cc_number = 0L;
        String cc_name = "";
        Date cc_expiry = null;
        String shipping = "";
        String street_1 = "";
        String street_2 = "";
        String city = "";
        String state = "";
        String zip = "";
        String country = "";
        
        Order result = Bookmarket.doBuyConfirm(storeId, shopping_id, customer_id, cc_type, 
        		cc_number, cc_name, cc_expiry, shipping, street_1, street_2, city, state, zip, country);
        Order expResult = Bookmarket.getMostRecentOrder(Bookmarket.getUserName(0));;
        assertEquals(expResult, result);
    }
    
    /**
     * Test of doBuyConfirm_14args method, of class Bookmarket. Throws a RuntimeException.
     */
    @Test(expected = RuntimeException.class)
    public void testDoBuyConfirm_14argsThrowsRuntimeException() {
      	System.out.println("doBuyConfirm_14argsThrowsRuntimeException");
      	int storeId = -1;
        int shopping_id = 0;
        int customer_id = 0;
        String cc_type = "";
        long cc_number = 0L;
        String cc_name = "";
        Date cc_expiry = null;
        String shipping = "";
        String street_1 = "";
        String street_2 = "";
        String city = "";
        String state = "";
        String zip = "";
        String country = "";
        Bookmarket.doBuyConfirm(storeId, shopping_id, customer_id, cc_type, 
        		cc_number, cc_name, cc_expiry, shipping, street_1, street_2, city, state, zip, country);
    }
    
    /**
     * Test of checkpoint method, of class Bookmarket.
     */
    @Test
    public void testCheckpoint() {
        System.out.println("checkpoint");

        Bookmarket mockedBookmarket = mock(Bookmarket.class);
        mockedBookmarket.checkpoint();

        Mockito.verify(mockedBookmarket).checkpoint();
    }
    
    /**
     * Test of rateABook method, of class Bookmarket.
     */
     @Test
    public void testRateABook() {
        System.out.println("rateABook");
        int storeId = 0;
        int shopping_id = Bookmarket.createEmptyCart(storeId);
        int customer_id = Bookmarket.getCustomer(Bookmarket.getUserName(0)).getId();
        Book book = Bookmarket.getABookAnyBook();
        Optional<BookRating> optional = Bookstore.getBookRating(customer_id, book.getId());

        while(optional.isPresent()) {
            book = Bookstore.getABookAnyBook(new Random(System.currentTimeMillis()));
            optional = Bookstore.getBookRating(customer_id, book.getId());
        }

        List<Integer> ids = new ArrayList<>();
        ids.add(Bookmarket.getABookAnyBook().getId());
        List<Integer> quantities = new ArrayList<>();
        quantities.add(1);
        Bookmarket.doCart(storeId, shopping_id, book.getId(), 
        		ids, quantities);
        String cc_type = "";
        long cc_number = 0L;
        String cc_name = "";
        Date cc_expiry = null;
        String shipping = "";
        
        // Create a new Order
        Bookmarket.doBuyConfirm(storeId, shopping_id, customer_id, cc_type, 
        		cc_number, cc_name, cc_expiry, shipping);
        
        // Avalia o livro comprado com nota menor que 0
        double rating = -0.1;
        BookRating result1 = Bookmarket.rateABook(customer_id, book.getId(), rating);
        assertEquals(null, result1);
        
        // Avalia o livro comprado com nota maior que 5
        rating = 5.1;
        BookRating result2 = Bookmarket.rateABook(customer_id, book.getId(), rating);
        assertEquals(null, result2);
        
        // Avalia o livro comprado com valor válido
        rating = 3.5;
        BookRating result3 = Bookmarket.rateABook(customer_id, book.getId(), rating);
        assertEquals(customer_id, result3.getCustomer().getId());
        assertEquals(book.getId(), result3.getBook().getId());
        assertEquals(rating, result3.getRating(), 0.0);
        
        // Atualiza a nota com nota menor que 0
        rating = -0.1;
        BookRating result4 = Bookmarket.rateABook(customer_id, book.getId(), rating);
        assertEquals(result3, result4);
        
        // Atualiza a nota com nota maior que 5
        rating = 5.1;
        BookRating result5 = Bookmarket.rateABook(customer_id, book.getId(), rating);
        assertEquals(result3, result5);

        // Atualiza a nota com nota válida.
        rating = 5.0;
        BookRating result6 = Bookmarket.rateABook(customer_id, book.getId(), rating);
        assertEquals(customer_id, result6.getCustomer().getId());
        assertEquals(book.getId(), result6.getBook().getId());
        assertEquals(rating, result6.getRating(), 5.0);
    }
    
    /**
     * Test of rateABook method, of class Bookmarket. Throws a RuntimeException.
     */
    @Test(expected = RuntimeException.class)
    public void testRateABookThrowsRuntimeException() {
        System.out.println("rateABookThrowsRuntimeException");
        int customer_id = -1;
        int book_id = -1;
        double rating = 0.0;
        Bookmarket.rateABook(customer_id, book_id, rating);
    }
}
