/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servico;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dominio.Address;
import dominio.Author;
import dominio.Book;
import dominio.BookRating;
import dominio.Cart;
import dominio.Customer;
import dominio.Order;
import dominio.Stock;


/**
 *
 * @author INF329
 */
public class BookstoreTest {
    
    public BookstoreTest() {
    }
    
    static Bookstore instance;
    
    @BeforeClass
    public static void setUpClass() {
    	instance = new Bookstore(0);
		long seed = System.currentTimeMillis();
		long now = System.currentTimeMillis();
		int items = 10000;
		int customers = 1000;
		int addresses = 1000;
		int authors = 100;
		int orders = 30000;
		instance.populate(seed, now, items, customers, addresses, authors);
		instance.populateInstanceBookstore(orders, new Random(seed), now);
		instance.populateEvaluation(instance.getOrdersById(), new Random(seed));
		instance.generateDataSetFile();
    }
    
    @AfterClass
    public static void tearDownClass() {
    	for (String file : instance.getDatasetFiles()) {
			try {
				Files.deleteIfExists(Paths.get(file));
			} catch (IOException e) {
				e.getMessage();
			}
		}
    	instance = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Bookstore.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Bookstore instance = BookstoreTest.instance;
        int expResult = 0;
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of isPopulated method, of class Bookstore.
     */
    @Test
    public void testIsPopulated() {
        System.out.println("isPopulated");
        Bookstore instance = BookstoreTest.instance;
        boolean expResult = true;
        boolean result = instance.isPopulated();
        assertEquals(expResult, result);
    }

    /**
     * Test of alwaysGetAddress method, of class Bookstore.
     */
    @Test
    public void testAlwaysGetAddress() {
        System.out.println("alwaysGetAddress");
        String street1 = "";
        String street2 = "";
        String city = "";
        String state = "";
        String zip = "";
        String countryName = "";
        Address result = instance.alwaysGetAddress(street1, street2, city, state, zip, countryName);
		Address expResult = new Address(0, street1, street2, city, state, zip, result.getCountry());
		assertEquals(expResult, result);
    }

    /**
     * Test of getCustomer method, of class Bookstore.
     */
    @Test
    public void testGetCustomer_int() {
        System.out.println("getCustomer");
        int cId = 0;
        Customer result = instance.getCustomer(cId);
		assertEquals(cId, result.getId());
    }

    /**
     * Test of getCustomer method, of class Bookstore.
     */
    @Test
    public void testGetCustomer_String() {
        System.out.println("getCustomer");
        String username = instance.getCustomer(10).getUname();
		Customer result = instance.getCustomer(username).get();
		assertEquals(username, result.getUname());
    }

    /**
     * Test of createCustomer method, of class Bookstore.
     */
    @Test
    public void testCreateCustomer() {
        System.out.println("createCustomer");
        String fname = "";
		String lname = "";
		String street1 = "";
		String street2 = "";
		String city = "";
		String state = "";
		String zip = "";
		String countryName = "";
		String phone = "";
		String email = "";
		double discount = 0.0;
		Date birthdate = null;
		String data = "";
		long now = 0L;

		Customer result = instance.createCustomer(fname, lname, street1, street2, city, state, zip, countryName, phone,
				email, discount, birthdate, data, now);
		int id = result.getId();
		String uname = result.getUname();
		Date since = result.getSince();
		Date lastVisit = result.getLastVisit();
		Date login = result.getLogin();
		Date expiration = result.getExpiration();
		Address address = result.getAddress();
		Customer expResult = new Customer(id, uname, uname.toLowerCase(), fname, lname, phone, email, since, lastVisit,
				login, expiration, discount, 0, 0, birthdate, data, address);
		assertEquals(expResult, result);
    }

    /**
     * Test of refreshCustomerSession method, of class Bookstore.
     */
    @Test
    public void testRefreshCustomerSession() {
        System.out.println("refreshCustomerSession");
        int cId = 0;
		long now = 0L;
		
		Date previousLogin = instance.getCustomer(cId).getLogin();
		Date previousExpiration = instance.getCustomer(cId).getExpiration();
		
		instance.refreshCustomerSession(cId, now);
		
		assertFalse(previousLogin.equals(instance.getCustomer(cId).getLogin()));
		assertFalse(previousExpiration.equals(instance.getCustomer(cId).getExpiration()));
    }

    /**
     * Test of getBook method, of class Bookstore.
     */
    @Test
    public void testGetBook() {
        System.out.println("getBook");
        int bId = 0;
		Book result = instance.getBook(bId).get();
		assertEquals(bId, result.getId());
    }
    
    /**
     * Test of getBookRatings method, of class Bookstore.
     */
    @Test
    public void testGetBookRatings() {
        System.out.println("getBookRatings");
        List<BookRating> expResult = null;
        List<BookRating> result = instance.getBookRatings();
        
        assertThat(result, is(not(expResult)));
    }

    /**
     * Test of getBookRating method, of class Bookstore.
     */
    @Test
    public void testGetBookRating() {
        System.out.println("getBookRating");
        int cId = 0;
        int bId = instance.getCustomer(cId).getMostRecentOrder().getLines().get(0).getBook().getId();
        List<BookRating> bookRatings = instance.getBookRatings();

        BookRating expResult = bookRatings.stream()
            .filter(bookRating -> bookRating.getCustomer().getId() == cId 
                && bookRating.getBook().getId() == bId)
            .findFirst().get();
        BookRating result = instance.getBookRating(cId, bId).get();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDatasetFiles method, of class Bookstore.
     */
    @Test
    public void testGetDatasetFiles() {
        System.out.println("getDatasetFiles");
        List<String> expResult = null;
        List<String> result = instance.getDatasetFiles();
        
        assertThat(result, is(not(expResult)));
    }

    /**
     * Test of getRecommendationByItens method, of class Bookstore.
     */
    @Test
    public void testGetRecommendationByItens() {
        System.out.println("getRecommendationByItens");
        int c_id = 0;
        List<Book> result1 = Bookstore.getRecommendationByItens(c_id);
        assertThat(result1, is(not(Collections.EMPTY_LIST)));
        
        int c_id2 = -1;
        List<Book> result2 = Bookstore.getRecommendationByItens(c_id2);
        assertThat(result2, is(Collections.EMPTY_LIST));
    }

    /**
     * Test of getRecommendationByUsers method, of class Bookstore.
     */
    @Test
    public void testGetRecommendationByUsers() {
        System.out.println("getRecommendationByUsers");
        int c_id = 0;
        List<Book> result1 = Bookstore.getRecommendationByUsers(c_id);
        assertThat(result1, is(not(Collections.EMPTY_LIST)));
        
        int c_id2 = -1;
        List<Book> result2 = Bookstore.getRecommendationByUsers(c_id2);
        assertThat(result2, is(Collections.EMPTY_LIST));
    }

    /**
     * Test of getABookAnyBook method, of class Bookstore.
     */
    @Test
    public void testGetABookAnyBook() {
        System.out.println("getABookAnyBook");
        Book result = Bookstore.getABookAnyBook(new Random(0));
        Book expResult = instance.getBook(result.getId()).get();
        assertEquals(expResult, result);
    }

    /**
     * Test of getBooksBySubject method, of class Bookstore.
     */
    @Test
    public void testGetBooksBySubject() {
        System.out.println("getBooksBySubject");
        String subject = "ARTS";
		List<Book> result = instance.getBooksBySubject(subject);
		assertEquals(true, result.stream().filter(book -> book.getSubject().equals(subject)).count() <= 51);
		assertTrue(result.stream().allMatch(b -> b.getSubject().equals(subject)));
    }

    /**
     * Test of getBooksByTitle method, of class Bookstore.
     */
    @Test
    public void testGetBooksByTitle() {
        System.out.println("getBooksByTitle");
        String title = instance.getBook(0).get().getTitle().substring(0, 4);
        Pattern regex = Pattern.compile("^" + title);
        
		List<Book> result = instance.getBooksByTitle(title);
		assertEquals(result.size(), result.stream().filter(book -> book.getTitle().startsWith(title)).count());
    }

    /**
     * Test of getBooksByAuthor method, of class Bookstore.
     */
    @Test
    public void testGetBooksByAuthor() {
        System.out.println("getBooksByAuthor");
        Author author = instance.getBook(0).get().getAuthor();
		List<Book> result = instance.getBooksByAuthor(author.getLname());
		assertEquals(result.size(),
				result.stream().filter(book -> book.getAuthor().getLname().equals(author.getLname())).count());
    }

    /**
     * Test of getNewBooks method, of class Bookstore.
     */
    @Test
    public void testGetNewBooks() {
        System.out.println("getNewBooks");
        String subject = instance.getBook(0).get().getSubject();
		List<Book> result = instance.getNewBooks(subject);
		assertEquals(result.size(), result.stream().filter(book -> book.getSubject().equals(subject)).count());
		
		String subject2 = "SUBJECT";
		List<Book> result2 = instance.getNewBooks(subject2);
		assertEquals(result2.size(), result2.stream().filter(book -> book.getSubject().equals(subject2)).count());
    }

    /**
     * Test of getNewBooks0 method, of class Bookstore.
     */
    @Test
    public void testGetNewBooks0() {
        System.out.println("getNewBooks0");
        String subject = instance.getBook(0).get().getSubject();
		List<Book> result = instance.getNewBooks0(subject);
		assertEquals(result.size(), result.stream().filter(book -> book.getSubject().equals(subject)).count());
		
		String subject2 = "SUBJECT";
		List<Book> result2 = instance.getNewBooks0(subject2);
		assertEquals(result2.size(), result2.stream().filter(book -> book.getSubject().equals(subject2)).count());
    }

    /**
     * Test of getBestSellers method, of class Bookstore.
     */
    @Test
    public void testGetBestSellers() {
        System.out.println("getBestSellers 01");
        Map<Book, Integer> result = instance.getBestSellers();
        assertEquals(true, result.size() <= 101);
    }

    /**
     * Test of getOrdersById method, of class Bookstore.
     */
    @Test
    public void testGetOrdersById() {
        System.out.println("getOrdersById");
        Bookstore bookstore = new Bookstore(1);
        List<Order> expResult1 = Collections.emptyList();
        List<Order> result1 = bookstore.getOrdersById();
        
        assertEquals(expResult1, result1);
        
        List<Order> result2 = instance.getOrdersById();
        Comparator<Order> byId = Comparator.comparingInt(Order::getId);
        
        assertEquals(true, isSorted(result2.toArray(), byId));
    }
    
    /**
     * Método auxiliar para verificar se um array está ordenado.
     * 
     * @param array um array a ser verificado.
     * @param comparator um <code>Comparator</code> para realizar a comparação.
     * @return true se está ordenado. false caso contrário.
     */
    private boolean isSorted(Object[] array, Comparator comparator) {
    	int length = array.length - 1;
	    for (int i = 0; i < length; ++i) {
	        if (comparator.compare(array[i], (array[i + 1])) > 0)
	            return false;
	    }
	 
	    return true;
	}

    /**
     * Test of updateBook method, of class Bookstore.
     */
    @Test
    public void testUpdateBook() {
        System.out.println("updateBook");
        int bId = 0;
		double cost = 0.0;
		String image = "";
		String thumbnail = "";
		long now = 0L;
		Book book = instance.getBook(bId).get();
		instance.updateBook(bId, image, thumbnail, now);
		assertEquals(bId, book.getId());
		assertEquals(image, book.getImage());
		assertEquals(thumbnail, book.getThumbnail());
    }

    /**
     * Test of updateStock method, of class Bookstore.
     */
    @Test
    public void testUpdateStock() {
        System.out.println("updateStock");
        int bId = 0;
        double cost = 0.0;
        Bookstore instance = BookstoreTest.instance;
        instance.updateStock(bId, cost);        

        assertEquals(cost, instance.getStock(bId).getCost(), 0);
    }

    /**
     * Test of getStock method, of class Bookstore.
     */
    @Test
    public void testGetStock() {
        System.out.println("getStock");
        int bId = 0;
        
        Stock expResult = null;
        Stock result = instance.getStock(bId);
        assertFalse(result.equals(expResult));
    }

    /**
     * Test of getCart method, of class Bookstore.
     */
    @Test
    public void testGetCart() {
        System.out.println("getCart");
        Cart cart = instance.createCart(System.currentTimeMillis());
		Cart result = instance.getCart(cart.getId());
		assertEquals(cart, result);
    }

    /**
     * Test of createCart method, of class Bookstore.
     */
    @Test
    public void testCreateCart() {
        System.out.println("createCart");
        long now = 0L;

		Cart result = instance.createCart(now);
		assertNotNull(result);
    }

    /**
     * Test of cartUpdate method, of class Bookstore.
     */
    @Test
    public void testCartUpdate() {
        System.out.println("cartUpdate");
        Cart cart = instance.createCart(System.currentTimeMillis());
		int cId = cart.getId();
		int bId = instance.getABookAnyBook(new Random(0)).getId();
		List<Integer> bIds = Arrays.asList(10, 20);
		List<Integer> quantities = Arrays.asList(10, 20);
		long now = System.currentTimeMillis();

		Cart expResult = cart;
		Cart result = instance.cartUpdate(cId, bId, bIds, quantities, now);
		assertEquals(expResult, result);
    }

    /**
     * Test of confirmBuy method, of class Bookstore.
     */
    @Test
    public void testConfirmBuy() {
        System.out.println("confirmBuy");
    	int customerId = 0;
		int cartId = 0;
		String comment = "";
		String ccType = "";
		long ccNumber = 0L;
		String ccName = "";
		Date ccExpiry = null;
		String shipping = "";
		Date shippingDate = null;
		int addressId = 0;
		long now = 0L;
		
		Order result = instance.confirmBuy(customerId, cartId, comment, ccType, ccNumber, ccName, ccExpiry, shipping,
				shippingDate, addressId, now);
		List<Order> orders = instance.getOrdersById();
		Order expResult = orders.get(orders.size() - 1);
		assertEquals(expResult, result);
    }

    /**
     * Test of populate method, of class Bookstore.
     */
    // @Test
    public void testPopulate() {
        System.out.println("populate");
        long seed = 0L;
        long now = 0L;
        int items = 0;
        int customers = 0;
        int addresses = 0;
        int authors = 0;
        boolean expResult = false;
        boolean result = Bookstore.populate(seed, now, items, customers, addresses, authors);
        assertEquals(expResult, result);
    }

    /**
     * Test of populateInstanceBookstore method, of class Bookstore.
     */
    @Test
    public void testPopulateInstanceBookstore() {
        System.out.println("populateInstanceBookstore");
        List<Order> expResult1      = null;
        Stock expResult2            = null;
        List<BookRating> expResult3 = null;
        List<Order> result1      = instance.getOrdersById();
		Stock result2            = instance.getStock(instance.getABookAnyBook(new Random(0)).getId());
		List<BookRating> result3 = instance.getBookRatings();
        
        assertThat(result1, is(not(expResult1)));
		assertThat(result2, is(not(expResult2)));
		assertThat(result3, is(not(expResult3)));
        
		result1 = instance.getOrdersById();
		result2 = instance.getStock(instance.getABookAnyBook(new Random(0)).getId());
		result3 = instance.getBookRatings();
		
		assertThat(result1, is(not(expResult1)));
		assertThat(result2, is(not(expResult2)));
		assertThat(result3, is(not(expResult3)));
    }
    
    /**
     * Test of populateEvaluation method, of class Bookstore.
     */
    @Test
    public void testPopulateEvaluation() {
    	System.out.println("populateEvaluation");
    	List<BookRating> bookRatingsById = instance.getBookRatings();
    	bookRatingsById.clear();
    	
    	assertEquals(0, bookRatingsById.size());
    	
    	instance.populateEvaluation(instance.getOrdersById(), new Random(0));
    	
    	assertNotEquals(instance.getBookRatings().toArray(), bookRatingsById.toArray());
    }
    
    /**
     * Test of generateDataSetFile method, of class Bookstore.
     */
    @Test
    public void testGenerateDataSetFile() {
    	System.out.println("generateDataSetFile");
    	int expResult = instance.getDatasetFiles().size();
    	instance.generateDataSetFile();
    	
    	assertEquals(expResult + 1, instance.getDatasetFiles().size());
    }
    
    /**
     * Test of rateABook method, of class Bookstore.
     */
    @Test
    public void testRateABook() {
    	System.out.println("rateABook");
    	int customer_id = instance.getCustomer(0).getId();
        Book book = instance.getABookAnyBook(new Random(System.currentTimeMillis()));
        Optional<BookRating> optional = instance.getBookRating(customer_id, book.getId());

        while(optional.isPresent()) {
            book = instance.getABookAnyBook(new Random(System.currentTimeMillis()));
            optional = instance.getBookRating(customer_id, book.getId());
        }
    	
    	// Avalia o livro comprado com nota menor que 0
        double rating = -0.1;
        BookRating result1 = instance.rateABook(customer_id, book.getId(), rating);
        assertEquals(null, result1);
        
        // Avalia o livro comprado com nota maior que 5
        rating = 5.1;
        BookRating result2 = instance.rateABook(customer_id, book.getId(), rating);
        assertEquals(null, result2);
        
        // Avalia o livro comprado com valor válido
        rating = 3.5;
        BookRating result3 = instance.rateABook(customer_id, book.getId(), rating);
        assertEquals(customer_id, result3.getCustomer().getId());
        assertEquals(book.getId(), result3.getBook().getId());
        assertEquals(rating, result3.getRating(), 0.0);
        
        // Atualiza a nota com nota menor que 0
        rating = -0.1;
        BookRating result4 = instance.rateABook(customer_id, book.getId(), rating);
        assertEquals(result3, result4);
        
        // Atualiza a nota com nota maior que 5
        rating = 5.1;
        BookRating result5 = instance.rateABook(customer_id, book.getId(), rating);
        assertEquals(result3, result5);

        // Atualiza a nota com nota válida.
        rating = 5.0;
        BookRating result6 = instance.rateABook(customer_id, book.getId(), rating);
        assertEquals(customer_id, result6.getCustomer().getId());
        assertEquals(book.getId(), result6.getBook().getId());
        assertEquals(rating, result6.getRating(), 5.0);
    }
}
